@component('mail::message')

<div>
    Добрый день, <b>{{ $user->fio  }}</b>
    <br>
    <br>
    Ваш логин: <b style="font-size: 18px;">{{ $user->email }}</b><br>
    Ваш пароль: <b style="font-size: 18px;">{{ $request->user_password }}</b><br>
</div>

@component('mail::button', ['url' => config('app.url'), 'color' => 'success'])
    На главную страницу
@endcomponent

@endcomponent
