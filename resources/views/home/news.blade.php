@extends('layouts.app')

@section('content')
    <main class="one-news">
        <div class="container">
            <div class="row pt-58 pb-90">

                <vue-main-news-link v-bind:news="{{ json_encode($news) }}"></vue-main-news-link>

                <div class="col-lg-8">
                    <div class="pt-16">
                        <span>
                            <img src="{{ $news->organization->logo }}" />
                            <span class="pl-10">{{ $news->organization->name }}</span>
                        </span>
                        <span class="ml-90 title gray">
                            <img src="/images/icons/calendar.svg" /> {{ \Carbon\Carbon::parse($news->publish_at)->translatedFormat('d F Y') }}
                        </span>
                    </div>
                    <div class="pt-10">
                        <div>
                            <a class="title black fs-40 fs-b ">{{ $news->title  }}</a>
                        </div>
                        <div class="pt-32">
                            <div>
                                <img src="{{ $news->image_conversions ? $news->image_conversions['large'] : '/images/seed/news_1_main_img.png' }}" />
                                {{--<div class="title gray center pt-16">Фото: Сергей Дробышев</div>--}}
                            </div>
                        </div>
                        {{ $news->text }}
                    </div>
                </div>
                <div class="col-lg-2 to-top-bottom">
                    <a class="btn" href="#"><img src="/images/icons/arrow-top.svg"/> Наверх</a>
                </div>
            </div>
        </div>
    </main>
@endsection