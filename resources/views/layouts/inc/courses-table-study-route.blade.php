<p>
@if (($events = $data['courses']->where('type', 'event'))->count())
	Мероприятия:
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Название</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($events as $event)
            <tr>
                <td>{{ $event->id }}</td>
                <td>{{ $event->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endif

@if (($courses = $data['courses']->whereIn('type', ['course', 'discipline']))->count())
	Курсы:
    <table>
        <thead>
            <tr>
               <th>Id</th>
               <th>Название</th>
            </tr>
        </thead>
        <tbody>

        @foreach ($courses as $course)
            <tr>
                <td>{{ $course->id }}</td>
                <td>{{ $course->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</p>

    <style>
        table {border: 1px solid #dee2e6;
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }

        table th,
        table td {
            border: 1px solid #dee2e6;
            padding: 0.75rem;
            vertical-align: top;
        }
        table tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }
    </style>
