<!DOCTYPE html>
<html lang="ru">
<head>
    @include('layouts.inc.test.head')
</head>
<body style="display: block;">
<main class="main container">
    <div class="" id="app">

        @yield('content')

    </div>
</main>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>