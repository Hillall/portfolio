<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

    @stack('scripts')

    <link rel="apple-touch-icon" sizes="180x180" href="/favicon_package/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon_package/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon_package/favicon-16x16.png">
    <link rel="manifest" href="/favicon_package/site.webmanifest">
    <link rel="mask-icon" href="/favicon_package/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
<div id="app">

    <!-- set progressbar -->
    <vue-progress-bar></vue-progress-bar>
    <vue-core></vue-core>
    <div id="pointPageTop" style="display: none"></div>

    <nav class="navbar navbar-expand-md navbar-light bg-main-color shadow-sm p-0 sticky-top">
        <div class="container">
            <a href="/" class="navbar-brand">
                <img src="/images/icons/head/logo.svg" width="80px" />
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="ml-auto p-2">

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <router-link :to="{ name: 'marketplace.index' }" class="nav-link">Курсы</router-link>
                        </li>

                        <li class="nav-item">
                            <router-link :to="{ name: 'org.list', params: { type: 'edu' }}" class="nav-link">Учреждения</router-link>
                        </li>

{{--                        <li class="nav-item">--}}
{{--                            <router-link :to="{ name: 'news.list' }" class="nav-link">Новости</router-link>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <router-link
                                :to="{ name: 'org.viro.about', params: { id: 1 }}"
                                class="nav-link">Сведения об организации</router-link>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="my-auto">
                <vue-main-nav-auth />
            </div>
            <div class="dropdown">
                <div
                    id="menuDropdown"
                    class="navbar-toggler m-2"
                    data-target="nav-link dropdown"
                    aria-controls="navbarSupportedContent"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <div class="navbar-toggler-icon"></div>
                    <div
                        class="dropdown-menu dropdown-menu-right"
                        aria-labelledby="menuDropdown"
                    >
                        <router-link :to="{ name: 'org.list', params: { type: 'edu' } }" class="dropdown-item">Учреждения</router-link>
                        <router-link
                            :to="{ name: 'org.viro.about', params: { id: 1 }}"
                            class="dropdown-item">Сведения об организации</router-link>
                    </div>
                </div>
            </div>

        </div>
    </nav>

    <div class="app-content">
        @yield('content')
    </div>

    <footer class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <img src="/images/icons/footer/tf.svg" />
                    <br>
                    <br>

                    <img src="/images/icons/footer/sc.svg"/>


                </div>
                <div class="col-lg-3">
                    <div class="footer-list fs-16">
                        <div class="head title gray">
                            О ВИРО
                        </div>
                        <div class="item">
                            <a class="title white" href="/organization/viro/1/about">О нас</a>
                        </div>
                    </div>
                </div>
{{--                <div class="col-lg-3">--}}
{{--                    <div class="footer-list fs-16">--}}
{{--                        <div class="head title gray">--}}
{{--                            ОБУЧЕНИЕ--}}
{{--                        </div>--}}
{{--                        <div class="item">--}}
{{--                            <a class="title white" href="/organization/list">Образовательные учреждения</a>--}}
{{--                        </div>--}}
{{--                        <div class="item">--}}
{{--                            <a class="title white">Педагоги</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3">--}}
{{--                    <div class="footer-list fs-16">--}}
{{--                        <div class="head title gray">--}}
{{--                            ПОМОЩЬ--}}
{{--                        </div>--}}
{{--                        --}}{{--                            <div class="item title white">--}}
{{--                        --}}{{--                                Написать в поддержку--}}
{{--                        --}}{{--                            </div>--}}
{{--                        --}}{{--                            <div class="item title white">--}}
{{--                        --}}{{--                                Часто задаваемые вопросы--}}
{{--                        --}}{{--                            </div>--}}
{{--                        <div class="item title white">--}}
{{--                            <img class="footer-icon" src="/images/icons/footer/phone.svg" />{{ $orgVIRO->phone ?? '' }}--}}
{{--                        </div>--}}
{{--                        <div class="item title white">--}}
{{--                            <img class="footer-icon" src="/images/icons/footer/mail.svg" />{{ $orgVIRO->email ?? '' }}--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-lg-12 pt-4 d-flex">
                    <div>
                        © ВИРО, 2020 <viro-version></viro-version>
                    </div>
                    <div class="ml-auto">
                        <a href="https://null-it.com" target="_blank" style="color: #9299A6"><span>Сделано в Null IT</span></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Scripts -->
{{--    <script src="{{ mix('/js/app.js') }}" defer></script>--}}
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
