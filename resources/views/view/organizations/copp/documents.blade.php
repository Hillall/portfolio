@extends('layouts.app')

@section('content')
    <div>
        <div class="head-edu">
            <div class="page-head edu-info">
                <div class="container pt-24 pb-40">
                    <div class="row">
                        <div class="col-lg-7 row no-gutters">
                            <div class="col-lg-3 col-xl-2">
                                <div class="logo"></div>
                            </div>
                            <div class="col-lg-9 col-xl-10 row no-gutters flex-column">
                                <div class="title gray fs-14">
                                    Организация
                                </div>
                                <div class="fs-32">

                                </div>
                                <div class="pt-32 row no-gutters align-items-center pb-10">
                                    <a
                                        href="#"
                                        class="btn btn-write mr-16"
                                        style="visibility: hidden;"
                                    >Написать</a>
                                    <a
                                        href="#"
                                        class="btn btn-user"
                                        style="visibility: hidden;"
                                    >Я работодатель</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-4" >
                            <div class="map d-flex align-items-end" style="background-image: url('/images/default-map.png')">
                                <div class="address">
                                    <!-- eslint-disable max-len -->
                                    <a
                                        target="_blank"
                                        class="title black"
                                        href="https://yandex.ru/maps/193/voronezh/?ll=39.207731%2C51.655502&mode=search&sll=39.207731%2C51.655502&text=Университетская площадь, 1&z=17"
                                    >
                                        <!-- eslint-disable max-len -->

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <nav class="nav flex-row flex-nowrap justify-content-center">
                        <li
                            class="nav-item"
                            {{--                            :class="$route.name == 'org.viro.program' ? 'active' : ''"--}}
                        >
                            <a
                                {{--                                :to="{ name: 'org.viro.program' }"--}}
                                class="nav-link"
                            >
                                Обр. программы
                            </a>
                        </li>
                        <li
                            class="nav-item"
                            {{--                            :class="$route.name == 'org.viro.teacher' ? 'active' : ''"--}}
                        >
                            <a
                                {{--                                :to="{ name: 'org.viro.teacher' }"--}}
                                class="nav-link"
                            >
                                Преподаватели
                            </a>
                        </li>
                        <li
                            class="nav-item"
                            {{--                            :class="$route.name == 'org.viro.about' ? 'active' : ''"--}}
                        >
                            <a
                                {{--                                :to="{ name: 'org.viro.about' }"--}}
                                class="nav-link"
                            >
                                О нас
                            </a>
                        </li>
                        <li
                            class="nav-item"
                            {{--                            :class="$route.name == 'org.viro.contact' ? 'active' : ''"--}}
                        >
                            <a
                                {{--                                :to="{ name: 'org.viro.contact' }"--}}
                                class="nav-link"
                            >
                                Контакты
                            </a>
                        </li>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
