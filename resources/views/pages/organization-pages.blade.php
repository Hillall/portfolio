@extends('layouts.app')
@section('content')

<main class="one-news">
    <div class="container">
        <div class="row pt-58 pb-90">
            <div class="col-lg-1">
                <div class="share-links">
                </div>
            </div>
            <div class="col-lg-8">
                <div class="pt-16">
					<span>
						<span class="pl-10">{{ $data['organization']->name }}</span>
					</span> 
					<span class="ml-90 title gray">
						<img src="/images/icons/calendar.svg"> {{ $data['page']->created_at }}
					</span>
				</div>
                <div class="pt-10">
                    <div><a class="title black fs-40 fs-b ">{{ $data['page']->title }}</a></div>
                    <div class="pt-16">
                        <p>{!! html_entity_decode($data['page']->content) !!}</p>
					</div>
					@if($data['page']->meta)
						<div>
							<span class="p-2 mb-5 badge badge-info text-white">{{ $data['page']->meta }}</span>
						</div>
					@endif
					@if($data['page']->documents)
						<div>
						@foreach($data['page']->documents as $document)
							<a href="{{ $document['url'] }}" download>{{ $document['name'] }}</a>
						@endforeach
						</div>
					@endif
                </div>
            </div>
            <div class="col-lg-2 to-top-bottom">
                <a href="javascript:void(0)" class="btn"><img src="/images/icons/arrow-top.svg"> Наверх</a>
            </div>
        </div>
    </div>
</main>
@endsection

