@extends('layouts.app')

@section('content')
<div class="container" style="min-height: 60vh !important;">
    <div class="row justify-content-center">
        <div class="col-md-8 py-5">
            <div class="card">
                <div class="card-header">Регистрация</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row ">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('messages.E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input
                                    id="email"
                                    type="email"
                                    placeholder="Разрешена почта заканчивающаяся на .com .ru .org"
                                    class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <vue-recaptcha prop_class="@error('g-recaptcha-response') is-invalid @enderror" @error('g-recaptcha-response') message="{{$message}}" @enderror></vue-recaptcha>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Регистрация
                                </button>
                            </div>
                        </div>
                        <div class="center mt-10">
                            <span>
                                Регистрируясь на сайте вы подтверждаете <a href="/policy.html" target="_blank">согласие на обработку данных</a>.
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
