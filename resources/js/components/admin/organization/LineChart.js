import VueChartJs from 'vue-chartjs'

export default {
    extends: VueChartJs.Bar,
    props: {
        chartdata: {
            type: Object,
            default: () => ({}),
        },
        options: {
            type: Object,
            default: null
        },
    },
    mounted () {
        this.renderChart(this.chartdata, this.options)
    }
}
