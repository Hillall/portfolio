// Bashinsky
operator = new Map([
    [ 'sum', '+' ],
    [ 'diff', '-' ],
    [ 'div', '/' ],
    [ 'mul', '*' ],
    [ 'pow', 'pow' ],
]);

class Formula {
    constructor(formula) {
        this.formula = _formula(formula);
    }

    _formula(formula) {
        if (formula.elements[0]) {
            return ''
        }
        return this._operator(formula, _formula());
    }

    _operator(formula, _cb) {
        const symbol = operator.get(formula.name)
        return formula.elements.reduce( (acc, curr) => acc + ' ' + symbol + ' ' + _cb(curr) );
    }
}