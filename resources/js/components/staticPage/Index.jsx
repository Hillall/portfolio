// Bashinsky
import { createNamespacedHelpers} from 'vuex';
import HomePageHeader  from './blocks/HomePageHeader';
import Slider from './blocks/Slider';

import Auth from '@base/components/main/Index';

import blockListCourse from './blocks/BlockListCourse';
import cardCourse from '@base/components/base/Card/cardCourse';

import BlockPopup from './manage/BlockPopup';
import { defBlock } from '@base/inc/defaults/app';
import WelcomepageBlockMenu from './blocks/menu';
import Banner from './blocks/Banner';
import statisticCard from '@base/components/base/statisticCard';

import verticaleMenu from '@base/components/base/verticaleMenu';

const welcomepage = createNamespacedHelpers('welcomepage');

export default {
    name: 'MainIndex',
    props: {
        typePage: {
            type: String,
            default: '',
        },
        page_id: {
            type: Number,
            default: 0,
        }
    },
    components: {
        HomePageHeader,
        Slider,
        blockListCourse,
        BlockPopup,
        WelcomepageBlockMenu,
        Banner,
        statisticCard,
        cardCourse,
        Auth,
    },
    data() {
        return {
            organizations: {},
            used_component: '',
            component_data: '',
            componentName: [
                { name: 'SLIDER', description: 'Слайдер', nameComponent: 'Slider', props: {} },
                { name: 'EDU', description: 'Курсы', nameComponent: 'Courses' },
                { name: 'BANNERS', description: 'Баннер', nameComponent: 'Banners' },
                { name: 'HTML', description: 'HTML', nameComponent: 'Html' },
                { name: 'STATISTIC', description: 'Статистика', nameComponent: 'Statistic' },
                { name: 'HEAD', description: 'Шапка', nameComponent: 'Head' },
                { name: 'AUTH', description: 'Авторизация', nameComponent: 'Auth' },
            ],
            currentBlock: {},
            visibleBlockPopup: false,
            selectedBlock: '',
            statisticName: {
                education_organization: 'Кол-во образовательных организаций' ,
                education_program: 'Кол-во образовательных программ',
                employer_organization: 'Кол-во работодателей',
                teachers: 'Кол-во преподавателей',
                users: 'Кол-во пользователей',
            },
            menuElements: [
                { class: "edit", action: "edit" },
                { class: "delete", action: "delete" },
            ]
        };
    },
    computed: {
        pageElements() {
            return this.$store.state.welcomepage.page;
        },
        pageId() {
            return this.$store.state.welcomepage.pageId;
        },
        permissions() {
            return this.$store.state.globalPermissions;
        },
    },
    created() {
        if (this.typePage === 'MAIN') {
            this.getMainPage();
        } else {
            this.getPage(this.page_id);
        }
    },
    mounted() {
    },
    methods: {
        ...welcomepage.mapActions({
            getMainPage: 'GET_MAIN_PAGE',
            getPage: 'GET_PAGE',
            addElement: 'ADD_ELEMENT_PAGE',
            deleteFile: 'DELETE_FILE',
            updateElement: 'UPDATE_ELEMENT_PAGE',
            deleteElement: 'DELETE_ELEMENT_PAGE',
            editElement: 'EDIT_ELEMENT_PAGE',
        }),
        showBlockPopup() {
            this.visibleBlockPopup = true;
        },
        closeBlockPopup() {
            this.visibleBlockPopup = false;
        },
        handleSetBlock(event) {
            this.selectedBlock = event.target.value;
        },
        editBlock(block) {
            this.currentBlock = block;
            this.visibleBlockPopup = true;
        },
        action(action, element) {
            switch (action) {
                case 'delete':
                    this.deleteElement({element_id: element.id});
                    break;
                case 'edit':
                    this.editBlock(element);
                    break;
            }
        },
        openPopupCreateBlock(selectedBlock) {
            if (selectedBlock) {
                if ( selectedBlock === 'HEAD' ) {
                    this.currentBlock = { ...defBlock, ...{ type: selectedBlock,
                                                            structure: { block: [], image: '', background: '', label: '', },
                                                            text: '',
                                                        }
                                        };
                } else {
                    this.currentBlock = { ...defBlock, ...{ type: selectedBlock } };
                }

                this.visibleBlockPopup = true;
            } else {
                console.log('Блок не выбран');
            }
        },
        updatePage() {
            if (this.typePage === 'MAIN') {
                this.getMainPage();
            } else if (this.typePage === 'STATIC') {
                this.getPage(this.page_id);
            }
        },

        async deleteBannerFile(payload) {
            try {
                await this.deleteFile(payload);
            } catch (e) {
                throw e;
            }
        },

        async createBlock(payload) {
            try {
                await this.addElement({ page_id: this.pageId, model: payload });
                this.updatePage();
            } catch (e) {
                console.warn(e);
                throw e;
            }
        },

        async updateBlock(payload) {
            //try {
                const noUnnecessaryFields = ({ welcome_page_id, updated_at, id, data, created_at, ...rest }) => rest;
                await this.updateElement({ page_id: payload.welcome_page_id, element_id: payload.id, model: noUnnecessaryFields(payload) });
                this.updatePage();
            // } catch (e) {
            //     console.warn(e);
            //     throw e;
            // }
        }

    },
    render(createElement) {
        const titleHeader = (label, text) => (<div class="head-main">
                                <div class="page-head viro-info">
                                    <div class="container pt-4 pb-5">
                                        <div class="">
                                            <div class="d-flex">
                                                <span
                                                    class="fs-32 px-2 block цвет-текста-1 fw-light pt-1 mb-1"
                                                >
                                                    <div domPropsInnerHTML={text}></div>
                                                </span>
                                            </div>
                                            <div class="d-flex">
                                                <span
                                                    class="fs-32 px-2 block цвет-текста-1 fw-light pt-1 mb-1"
                                                >
                                                    { label }
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>);

        const statisticTemplate = (statistics) => Object.keys(statistics).map(el => <div class="col-lg-3 mb-4"><statisticCard name={this.statisticName[el]} total={statistics[el]} ></statisticCard></div>);

        const courseTemplate = (courses) => courses.map((element) => { return <div class="course col-lg-3"><cardCourse course={element} /></div>});

        const courseBlock = (courses) => <blockListCourse
            blockName={'Курсы'}
            routerName={'marketplace.index'}
            classStyle={'row courses'}
            {...{
                scopedSlots: {
                    listElement: () => {
                        return (<div class="row">{courseTemplate(courses)}</div>)
                    }
                }
            }}
        />

        const welcomePageTemplate = this.pageElements.map(el => {
            switch (el.type) {
                case 'SLIDER':
                    return (<div class="container">
                        <slider sliderElements={el.structure} class={ 'page__block' }
                        {...{
                            scopedSlots: {
                                control: () => {
                                    if (this.permissions.moderation_welcomepage) return <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative slider' } menuElements={ this.menuElements } element={el}/>
                                }
                            }
                        }}
                    /></div>);
                case 'NEWS':
                    return (<div class="container"><div class="page__block">
                        { this.permissions.moderation_welcomepage ?
                            <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative' } menuElements={ this.menuElements } element={el}/> : ''
                        }
                        {newsBlock(el)}
                    </div></div>)
                case 'EDU':
                    return (<div class="container"><div class="page__block">
                        { this.permissions.moderation_welcomepage ?
                            <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative' } menuElements={ this.menuElements } element={el}/> : ''
                        }
                        {courseBlock(el.data)}
                    </div></div>)
                case 'BANNERS':
                    return (<div class="container">
                        <div class="page__block">
                        { this.permissions.moderation_welcomepage ?
                            <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative' } menuElements={ this.menuElements } element={el}/> : ''
                        }
                        <div class="row">
                            {Object.keys(el.structure).map(k => {
                                    return <banner class="mt-5" banner={el.structure[k]}></banner>
                                })}
                        </div>
                            <div class="row">

                                {el.data ? <div class="col-lg-12 row mt-4 no-gutters">
                                    {el.data.map(el => {
                                        return <div class="document-item file">
                                            <a href={el.url}>{el.id.name}</a>
                                        </div>
                                    })}
                                </div> : {}}
                            </div>
                        </div>
                        </div>
                    )
                case 'HTML':
                    return (<div class="container">
                                <div class="page__block">
                                    { this.permissions.moderation_welcomepage ?
                                        <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative' } menuElements={ this.menuElements } element={el}/> : ''
                                    }
                                    <div >
                                        {createElement("div", { domProps: {innerHTML: el.structure.html}})}
                                    </div>
                                </div>
                            </div>)
                case 'STATISTIC':
                    return  (<div class="container">
                                <div class="page__block">
                                        { this.permissions.moderation_welcomepage ?
                                            <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative' } menuElements={ this.menuElements } element={el}/> : ''
                                        }
                                    <div class="row">
                                        {statisticTemplate(el.data)}
                                    </div>
                                </div>
                            </div>)
                case 'AUTH':
                    return (<div class="container"><div class="page__block">
                        {this.permissions.moderation_welcomepage ? <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative slider' } menuElements={ this.menuElements } element={el}/> : ''}
                        <div className="row">
                            <auth structure={el.structure}/>
                        </div>
                    </div>
                </div>)
                case 'HEAD':
                    return (<div class="page__block">
                    {this.permissions.moderation_welcomepage ? <verticaleMenu onAction = { this.action } classStyle={ 'menu-relative slider' } menuElements={ this.menuElements } element={el}/> : ''}
                        { titleHeader(el.structure.label, el.structure.text ) } </div>)
            }
        })
        return (
            <div>
                {this.permissions.moderation_welcomepage ? <div class="container">
                        <nav class="navbar bg-white shadow-lg mt-50 fixed-bottom" style="z-index: 1000">
                            <div class="container">
                                <div class="col-lg-12">
                                    <form class="form-inline">
                                        {this.componentName.map((el) =>
                                            <button class="btn btn--main ml-2 mr-2" type="button"
                                                    onClick={() => this.openPopupCreateBlock(el.name)}>
                                                <span class="text-color-white">{el.description}</span>
                                            </button>)
                                        }
                                    </form>
                                </div>
                            </div>
                        </nav>
                    </div> : ''
                }

                <main class="main">
                    {welcomePageTemplate}
                    { this.visibleBlockPopup ? <BlockPopup
                                                    onclosePopup = {this.closeBlockPopup}
                                                    createBlock={this.createBlock}
                                                    block={this.currentBlock}
                                                    updateBlock={this.updateBlock}
                                                    deleteFile={this.deleteBannerFile}/> : '' }
                </main>
            </div>
        )
    }
}
