/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import Button from 'vue-progress-button';
import VueProgressBar from 'vue-progressbar';
//import VueMoment from 'vue-moment';
import VueScrollTo from 'vue-scrollto';
import VuejsDialog from 'vuejs-dialog';
import VueBootstrapTypeahead from 'vue-bootstrap-typeahead';
import { BootstrapVue } from 'bootstrap-vue'

import VTooltip from 'v-tooltip';
import { ServerTable } from 'vue-tables-2';
import axios from '@base/bootstrap';
import store from '@base/store/app';
import router from '@base/route/app';
import Core from '@base/components/Core';


import VtSortControl from '@base/components/inc/VueTable/VtSortControl';
import VueMoment from 'vue-moment'
import moment from 'moment'
import 'moment/locale/ru'

import '@base/filters/filesize';
import VueYandexMetrika from 'vue-yandex-metrika'
import {Chrome} from 'vue-color';
import VCalendar from 'v-calendar';
import checkView from 'vue-check-view'
Vue.use(checkView)

window.Vue = Vue;

Vue.use(BootstrapVue)
Vue.prototype.$moment = VueMoment;
Vue.use(VueMoment, {
    moment,
})
Vue.moment.locale('ru')
Vue.use(VCalendar);

axios.interceptors.request.use((config) => {
    store.dispatch('animateLoading', { type: 'local', action: 'start' }, { root: true });
    return config;
    // return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    store.dispatch('animateLoading', { type: 'local', action: 'finish' }, { root: true });
    return response;
}, function (error) {
    store.dispatch('animateLoading', { type: 'local', action: 'error', status_code: error.request.status }, { root: true });
    return Promise.reject(error);
});


Vue.prototype.$axios = axios;
Vue.use(VueProgressBar, {
    color: '#F85A40',
    failedColor: 'red',
    height: '3px',
    autoFinish: true,
});

Vue.use(VueScrollTo);
Vue.use(VuejsDialog);
Vue.use(VTooltip);
Vue.component('vue-bootstrap-typeahead', VueBootstrapTypeahead);

const options = {};
const useVuex = false;
const theme = 'bootstrap4';
const template = { sortControl: VtSortControl };
Vue.use(ServerTable, options, useVuex, theme, template);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('vue-progress-button', Button);
Vue.component('vue-pagination', require('laravel-vue-pagination'));
Vue.component('vue-main-nav-auth', require('./components/main/login/Auth.vue').default);
Vue.component('vue-recaptcha', require('./components/main/register/recaptcha').default);
Vue.component('viro-version', require('./components/main/version').default);
Vue.component('color-picker', Chrome);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueYandexMetrika, {
    id: 67721788,
    router: router,
    env: process.env.NODE_ENV
    // other options
})


const app = new Vue({ // eslint-disable-line no-unused-vars
    el: '#app',
    store,
    router,
    components: {
        VueCore: Core,
    },
});

