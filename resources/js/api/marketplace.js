import axios from '@base/bootstrap';

// eslint-disable-next-line no-unused-vars
function getCourses(payload) {
    return axios.get('/api/v1/courses/', {
        params: {
            order_by: 'popularity',
            status: 'active',
            type: (payload.type === 'all') ? null : payload.type,
            show: payload.show ? payload.show : 1,
            search: payload.search ? payload.search : '',
            organization_id: payload.organization_id ? payload.organization_id : '',
            qty: payload.qty ? payload.qty : 15,
            page: payload.page ? payload.page : 1,
            attempt_id: payload.attempt_id ? payload.attempt_id : null,
            action: payload.action ? payload.action : null,
            can_iom: payload.can_iom ? payload.can_iom : null,
            certificate: payload.certificate ? payload.certificate : null,
            card_filters: payload.card_filters ? payload.card_filters : null,
        },
    })
        .then((response) => response.data);
}

export {
    getCourses,
};

export default {};
