// Bashinsky

import axios from '@base/bootstrap';

const URL_STUDY_ROUTE = '/api/v1/study-routes/';
const URL_STUDY_ROUTE_ANALITIC = 'api/v1/study-route-analitic'
const _headerAppJson = { headers: { Accept: 'application/json' } };

const formData = (payload) => {
    let formData = {
        '_method': 'patch',
    }
        formData = { ...formData, ...{ [`${payload.type}_id`]: payload.course_id,
                                        action: `${payload.action}-${payload.type}`,
                                        comment: payload.comment
                                        } }
    return formData;
}

export async function attachCourse(route_id, course_id, type) {
    let data = formData({ course_id: course_id, type: type, action: 'attach' });
    return await axios.post(URL_STUDY_ROUTE + route_id, data, _headerAppJson)
}


export async function detachCourse(route_id, course_id, type, comment) {
    let data = formData({ course_id: course_id, type: type, action: 'detach', comment: comment });
    return await axios.post(URL_STUDY_ROUTE + route_id, data, _headerAppJson)
}



export async function decline(payload) {
    const formData = {
      '_method': 'patch',
      'action': 'decline',
      'step': payload.step,
      'comment': payload.comment || '',
    };

    return await axios.post(URL_STUDY_ROUTE + payload.id, formData, _headerAppJson)
}

export async function accept(payload) {
    const formData = {
        '_method': 'patch',
        'action': 'accept',
        'comment': payload.comment || '',
    };
    return await axios.post(URL_STUDY_ROUTE + payload.id, formData, _headerAppJson)
}

export async function createExternalCourse(route_id, payload) {
    const formData = {
        "_method": 'patch',
        "action": "create-external-course",
        ...payload
    };

    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson);
}

export async function updateExternalCourse(route_id, payload) {
    const noId = ({ id , ...rest}) => rest;
    const formData = {
        "_method": 'patch',
        "action": "update-external-course",
        ...{ 'external_course_id': payload.id },
        ...noId(payload)
    };

    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson);
}

export async function deleteExternalCourse(route_id, external_course_id) {
    const formData = {
        "_method": "patch",
        "action": "delete-external-course",
        "external_course_id": external_course_id,
    }

    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson)
}

export async function attachDocumensts(route_id, documents) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'attach-documents');
    for (let document of documents) {
        formData.append('documents[]', document);
    }
    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson);
}

export async function detachDocuments(route_id, document_id) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'detach-document');
    formData.append('document_id', document_id);
    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson)
}

export async function saveFormData(route_id, form_fields_value) {
    const formData = {
        '_method': 'patch',
        'action': 'save-form-data',
        'form_fields_value': form_fields_value,
    }
    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson)
}

export async function userRoutes(payload) {
    let params = {
        action: 'user_routes',
        paginated: 0,
        ...payload
    }

    if (payload.id) {
        params.id = payload.id;
    }

    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function reviewRoutes(payload) {
    let params = {
        action: 'review-routes',
        ...payload,
    }
    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function approvingRoute(payload) {
    let params = {
        action: 'study-routes-approving',
        ...payload,
    }
    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function routesByAttachedRegions(payload) {
    let params = {
        action: 'routes-by-attached-regions',
        ...payload,
    }
    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function studyRoutesApproving(payload) {
    let params = {
        action: 'study-routes-approving',
        ...payload,
    }
    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function allRoutes(payload) {
    let params = payload;
    params.action = 'all-routes';
    return await axios.get(URL_STUDY_ROUTE, { params: params });
}

export async function authorizedUsersRoutes(payload) {
    let params = {
        qty: 15,
        page: payload.page,
        paginated: 1,
        with: [ 'user', 'courses' ]
    }
    if (payload.filter) {
        params = {
            ...params, ...filter
        }
    }
    if (payload.order_by) {
        params.order_by = payload.order_by;
    }
    return await axios.get(URL_STUDY_ROUTE, );
}

export async function downloadDocument(route_id) {
    return await axios.get(URL_STUDY_ROUTE + route_id + '/doc-template');
}

export async function setScan(route_id, scan) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-scan');
    formData.append('scan', scan)
    return await axios.post(URL_STUDY_ROUTE + route_id, formData, _headerAppJson);
}

export async function analiticRoutesByPeriod(payload) {
    let params = {
        action: 'by-period',
        range_start: payload.range_start,
    }

    if (payload.range_end) {
        params = { ...params, ...{ range_end:  payload.range_end }};
    }

    return await axios.get(URL_STUDY_ROUTE_ANALITIC,  params );
}

export async function analiticSelectedRoutes(ids) {
    const params = {
        action: 'custom-ids',
        ids: ids,
    }
    return await axios.get(URL_STUDY_ROUTE_ANALITIC,  params );
}

export async function addReview(text) {
    const params = {
        '_method': 'patch',
        action: 'add-review',
        review: text,
    }
    return await axios.get(URL_STUDY_ROUTE, params);
}

export async function  setFinished(route_id) {
    const params = {
        '_method': 'patch',
        action: 'set-finished',
    }
    return await axios.get(URL_STUDY_ROUTE + route_id, params);
}

const _courses = (action, courses) => {
    return {
        '_method': 'patch',
        action: action,
        courses: courses,
    }
}

export async function acceptCourses(courses, route_id) {
    return await axios.post(URL_STUDY_ROUTE + route_id, _courses('accept-courses', courses));
}

export async function rejectCourses(courses, route_id) {
    return await axios.post(URL_STUDY_ROUTE + route_id, _courses('reject-courses', courses));
}

export async function allowSpeaker(courses, route_id) {
    return await axios.post(URL_STUDY_ROUTE + route_id, _courses('allow-speaker', courses));
}

export async function forbidSpeaker(courses, route_id) {
    return await axios.post(URL_STUDY_ROUTE + route_id, _courses('forbid-speaker', courses));
}

export async function endCourses(courses, route_id) {
    return await axios.post(URL_STUDY_ROUTE + route_id, _courses('ended-courses', courses));
}
