// Bashinsky

import axios from '@base/bootstrap';

const URL_BASKET = '/api/v1/study-route-basket';

export async function checkout(organization_id, quiz_attempt_id) {
    const payload = {
        action: "checkout",
        organization_id: organization_id,
        quiz_attempt_id: quiz_attempt_id,
    }
    return await axios.post(URL_BASKET, payload, { headers: { Accept: 'application/json' } })
}

export async function addToRequest(payload) {
    const data = {
        action: 'add-to-request',
        type: payload.type,
        id: payload.id,
    }
    return await axios.post(URL_BASKET, data, { headers: { Accept: 'application/json' } })
}

export async function delFromRequest(payload) {
    const data = {
        action: 'del-from-request',
        type: payload.type,
        id: payload.id,
    }
    return await axios.post(URL_BASKET, data, { headers: { Accept: 'application/json' } });
}

export async function getBasket() {
    return await axios.get(URL_BASKET, { headers: { Accept: 'application/json' } });
}

