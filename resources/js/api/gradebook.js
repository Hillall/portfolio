import axios from '@base/bootstrap';

const URL_GRADEBOOK = '/api/v1/gradebooks';

async function addScore(payload) {
    const formData = new FormData();
    formData.append('action', 'add-score');
    formData.append('user_id', payload.user_id);
    // formData.append('group_id', payload.group_id);
    formData.append('lesson_id', payload.lesson_id);
    formData.append('score',payload.score );
    return axios.post(URL_GRADEBOOK, formData)
        .then((res) => res);
}

function delScore(payload) {
    const formData = new FormData();
    formData.append('action', 'del-score');
    formData.append('user_id', payload.user_id);
    // formData.append('group_id', payload.group_id);
    formData.append('lesson_id', payload.lesson_id);
    formData.append('score',payload.score );
    return axios.post(URL_GRADEBOOK, formData)
        .then((res) => res);
}

async function setScores(payload) {
    const formData = new FormData();
    formData.append('action', 'set-scores');
    formData.append('user_id', payload.user_id);
    // formData.append('group_id', payload.group_id);
    formData.append('lesson_id', payload.lesson_id);
    formData.append('scores[]', payload.scores);
    return axios.post(URL_GRADEBOOK, formData)
        .then((res) => res );
}

async function setPresence(payload) {
    const formData = new FormData();
    formData.append('action', 'set-presence');
    formData.append('user_id', payload.user_id);
    // formData.append('group_id', payload.group_id);
    formData.append('lesson_id', payload.lesson_id);
    formData.append('presence', payload.presence);
    return axios.post(URL_GRADEBOOK, formData)
        .then((res) => res );
}

async function getGradeBook(payload) {
    return axios.get(URL_GRADEBOOK, { params: { ...payload } })
        .then(res => res);
}

export {
    setScores,
    setPresence,
    delScore,
    addScore,
    getGradeBook,
};
