// Bashinsky

import axios from '@base/bootstrap';

const URL_USER = '/api/v1/users/';
const URL_USER_QUIZ = '/api/v1/me/quiz';

async function getUsers(payload) {
    return axios.get(URL_USER, { params: payload })
                .then((response) => response.data.data.users);
}

async function getUser(user_id, payload) {
    return axios.get(URL_USER + user_id, { params: payload })
                .then((response) => response.data.data.user);
}

async function createUser(payload) {
    return axios.post(URL_USER, payload)
        .then((response) => response.data.data.user);
}

async function updateUser(user_id, payload) {
    return axios.post(URL_USER + user_id, payload)
        .then((response) => response.data.data.user);
}

async function getUserQuizzes(payload) {
    return axios.get(URL_USER_QUIZ, { params: payload })
        .then((response) => response)
        .catch((error) => error.response);
}

async function getUserQuizzesByCards(payload) {
    return axios.get(URL_USER_QUIZ + '/by_card', { params: payload })
        .then((response) => response)
        .catch((error) => error.response);
}


async function getVacancyResponses(data) {

    return axios.get(URL_USER + 'vacancy/responses', { params: data })
        .then((response) => response.data.data);
}




export {
    URL_USER,
    getUsers,
    getUser,
    createUser,
    updateUser,
    getUserQuizzes,
    getUserQuizzesByCards,
    getVacancyResponses
}
