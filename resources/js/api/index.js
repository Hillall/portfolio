import axios from '@base/bootstrap';

function getPrograms(payload, store) {

    return axios.get('/api/v1/programs/', {
        params: {
            order_by: 'popularity',
            order: payload.order ? payload.order : 'desc',
            show: payload.show ? payload.show : 1,
            search: payload.search ? payload.search : '',
            groups: payload.group_type ? payload.group_type : '',
            speciality_type: payload.speciality_type ? payload.speciality_type : '',
            type: (payload.type === 'all') ? null : payload.type,
            organization_id: payload.organization_id ? payload.organization_id : '',
            qty: payload.qty ? payload.qty : 15,
            page: payload.page ? payload.page : 1,
        },
    },
    { headers: { 'Content-Type': 'multipart/form-data' } })
        .then((response) => {
            const { programs } = response.data.data;

            return programs;
        })
        .catch(() => {

        });
}
function getMyPrograms(payload, store) {

    return axios.get('/api/v1/programs/my', {
        params: {
            order_by: 'popularity',
            order: payload.order ? payload.order : 'desc',
            show: payload.show ? payload.show : 1,
            search: payload.search ? payload.search : '',
            type: (payload.type === 'all') ? null : payload.type,
            organization_id: payload.organization_id ? payload.organization_id : '',
            qty: payload.qty ? payload.qty : 15,
            page: payload.page ? payload.page : 1,
        },
    },
    { headers: { 'Content-Type': 'multipart/form-data' } })
        .then((response) => {
            const { programs } = response.data.data;

            return programs;
        });
}


function attachOrDetachUser(payload, action) { // eslint-disable-line
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', action);
    formData.append('user_id', payload.user_id);

    return axios.post(`/api/v1/${payload.to}/${payload.id}`, formData,
        { headers: { 'Content-Type': 'multipart/form-data' } });
}

function detailedLesson(id) {
    return axios.get(`/api/v1/lessons/${id}`);
}

export {
    getPrograms,
    getMyPrograms,
    attachOrDetachUser,
    detailedLesson,
};
