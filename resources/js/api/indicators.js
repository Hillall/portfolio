// Bashinsky

import axios from '@base/bootstrap';

const URL_INDICATORS = '/api/v1/indicator/';

function getIndicators(data){
    return axios.get(URL_INDICATORS, { params: data })
        .then((response) => response);
}

function getIndicator(id, data){
    return axios.get(URL_INDICATORS + id, { params: data })
        .then((response) => response);
}

async function updateIndicator(id, data){
    let formData = { action: 'data', '_method': 'patch', ...data };
    return await axios.post(URL_INDICATORS + id, formData);
}

function createIndicator(data) {
    return axios.post(URL_INDICATORS, data )
        .then((response) => response);
}
function deleteIndicator(data){
    return axios.post(URL_INDICATORS + data.id, {
        _method: 'DELETE'
    }).then((response) => response);
}

export {
    getIndicators,
    getIndicator,
    updateIndicator,
    createIndicator,
    deleteIndicator
}
