// Bashinsky

import axios from '@base/bootstrap';

const URL_STUDY_ROUTE_FORMS = '/api/v1/study-route-forms/';

const _headerAppJson = { headers: { Accept: 'application/json' } };

async function _fieldData(payload) {
    let formData = {
        '_method': 'patch',
        'action': payload.action,
        'name': payload.name,
        'type': payload.type,
        'var_name': payload.var_name,
    }
    if (payload.type === 'select') {
        formData = { ...formData, ...{ data: payload.data }};
    }

    if (payload.action === 'update-field') {
        formData = { ...formData, ...{ 'field_id': payload.id }};
    }

    return formData;
}

export async function getForms(){
    return await axios.get(URL_STUDY_ROUTE_FORMS, { params: { 'with[]': 'fields' } });
}

export async function createForms(name) {
    const formData = new FormData();
    formData.append('name', name);
    return await axios.post(URL_STUDY_ROUTE_FORMS, formData, _headerAppJson);
}

export async function updateForms(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    formData.append('name', payload.name);
    formData.append('doc_template', payload.doc_template);
    return await axios.post(URL_STUDY_ROUTE_FORMS + payload.id, formData,  _headerAppJson );
}

export async function setActive(form_id) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-active');
    return await axios.post(URL_STUDY_ROUTE_FORMS + form_id, formData, _headerAppJson );
}

export async function createField({form_id, value}) {
    const formData = await _fieldData({ ...value, action: 'add-field' })
    // console.log(formData);
    return await axios.post(URL_STUDY_ROUTE_FORMS + form_id, formData, _headerAppJson );
}


export async function updateField({form_id, value}) {
    const formData = await _fieldData({ ...value, action: 'update-field' })
    return await axios.post(URL_STUDY_ROUTE_FORMS + form_id, formData, _headerAppJson );
}

export async function deleteField({ field_id, form_id }) {
    const formData = new FormData();

    formData.append('_method', 'patch');
    formData.append('action', 'delete-field');
    formData.append('field_id', field_id);

    return await axios.post(URL_STUDY_ROUTE_FORMS + form_id, formData, _headerAppJson );
}
