import axios from '@base/bootstrap';

function getProfiles() {
    return axios.get('/api/v1/profiles/').then((response) => {
        return response;
    });
}

export {
    getProfiles
};
