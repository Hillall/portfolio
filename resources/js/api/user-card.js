import axios from '@base/bootstrap';
const URL_USER_CARD = '/api/v2/user-card';
const URL_USER_CARD_TEMPLATE = '/api/v2/user-card-template/';

async function getCards(payload) {
    return axios.get(URL_USER_CARD, { params: payload })
        .then((response) => response.data.data);
}

async function createCard(payload){
    return axios.post(URL_USER_CARD, payload )
        .then((response) => response.data.data);
}


async function getCardTemplate(payload) {
    return axios.get(URL_USER_CARD_TEMPLATE, { params: payload })
        .then((response) => response.data.data);
}

async function getCardTemplateById(id, payload) {
    return axios.get(URL_USER_CARD_TEMPLATE + id, { params: payload })
        .then((response) => response.data.data);
}

async function createCardTemplate(payload){
    return axios.post(URL_USER_CARD_TEMPLATE, payload )
        .then((response) => response.data.data);
}

async function createCardTemplateField(card_template_id, payload){
    return axios.post(URL_USER_CARD_TEMPLATE + card_template_id, payload )
        .then((response) => response.data.data);
}




export {
    URL_USER_CARD,
    URL_USER_CARD_TEMPLATE,
    getCards,
    createCard,
    getCardTemplate,
    createCardTemplate,
    getCardTemplateById,
    createCardTemplateField,
}
