// Bashinsky

import axios from '@base/bootstrap';
const URL_QUIZ_ACCESS='/api/v1/quiz-access/';

async function getOrganizationAccess(payload){
    return axios.get(URL_QUIZ_ACCESS, { params : payload   })
        .then(response => response.data);
}

export {
    URL_QUIZ_ACCESS,
    getOrganizationAccess
}
