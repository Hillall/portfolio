// Bashinsky

import axios from '@base/bootstrap';

const URL_ATTEMPT_QUESTION = '/api/v1/quiz-attempt-questions/';
const URL_ATTEMPT_RESULT = '/api/v1/quiz-attempt-result/';
const URL_QUIZ_ATTEMPTS = '/api/v1/quiz-attempts/';

async function answer(id, payload) {
    const params = {
        '_method': 'patch',
        'answer': payload,
    }
    return axios.post(URL_ATTEMPT_QUESTION + id, params, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function answerTextAudiosFiles(id, payload) {
    let data = new FormData();
    data.append('_method', 'patch');
    data.append('answer', payload.text)
    payload.audio.forEach(el => {
        data.append('answer_audio[]', el.blob, new Date().getTime()+'.wav');
    })
    payload.files.forEach(el => {
        data.append('answer_files[]', el)
    })

    return axios.post(URL_ATTEMPT_QUESTION + id, data, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function deleteAudioFileAnswer(id, file_id){
    let data = new FormData();
    data.append('_method', 'patch');
    data.append('action', 'delete-file');
    data.append('file_id', file_id);

    return axios.post(URL_ATTEMPT_QUESTION + id, data, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function quizAttempts(payload) {
    return axios.get(URL_QUIZ_ATTEMPTS, { params : { ...payload  } })
                .then(response => response.data.data.attempts.data[0]);
}

async function resultOfUserAttempt(payload) {
    const params = {
        'with': payload.with ? ['attempt_result', ...payload.with] : ['attempt_result'],
        'quiz_id': payload.quiz_id,
        'organization_id': payload.organization_id,
        'user_id': payload.user_id,
        'qty': payload.qty,
        'page': payload.page,
        'paginated': payload.paginated,
        ...payload.filters
    }
    return axios.get(URL_QUIZ_ATTEMPTS, { params: params }, { Accept: 'application/json' })
                .then(response => response.data.data.attempts);
}

async function finishAttempt(id) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'finish-attempt');
    return axios.post(URL_QUIZ_ATTEMPTS + id, formData, { Accept: 'application/json' })
                .then(response => response.data.data.attempt);
}

async function getAnswerCheck(payload) {
    const params = {
        'without_result': 1,
        'with': ['answer', 'attempt-with-user'],
        'state': 'finished',
        'types': ['text'],
        'attempt_id': payload.attempt_id,
    }

    return axios.get(URL_ATTEMPT_QUESTION, { params: params },  { Accept: 'application/json' } )
                .then(response => {
                    return response.data.data;
                });
}

async function setGrade(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-score-text-answer');
    formData.append('answer_id', payload.answer_id);
    formData.append('score', payload.score);

    return axios.post(URL_QUIZ_ATTEMPTS + payload.attempt_id,  formData,  { Accept: 'application/json' })
                .then((response) => {
                })
}

async function quizAttemptDelete(payload) {
    return axios.post(URL_QUIZ_ATTEMPTS + payload.attempt_id, {
            _method: 'DELETE'
        })
        .then(response => response.data.data);
}

export {
    URL_ATTEMPT_QUESTION,
    URL_ATTEMPT_RESULT,
    URL_QUIZ_ATTEMPTS,
    answer,
    answerTextAudiosFiles,
    deleteAudioFileAnswer,
    quizAttempts,
    finishAttempt,
    getAnswerCheck,
    setGrade,
    resultOfUserAttempt,
    quizAttemptDelete
}
