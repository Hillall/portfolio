// Bashinsky

import axios from '@base/bootstrap';

const URL_QUIZZES = '/api/v1/quizzes/';

async function addQuestionSpecific(payload) {
    const formData = new FormData();
    formData.append('action', 'add-question-specific');
    formData.append('question_id', payload.question_id);
    formData.append('point_count', payload.point_count);
    formData.append('_method', 'patch');
    return axios.post(URL_QUIZZES + payload.id, formData);
}

async function addQuestionRandom(payload) {
    const formData = new FormData();
    formData.append('action', 'add-question-random');
    formData.append('bank_id', payload.bank_id);
    formData.append('question_count', payload.question_count);
    formData.append('point_count', payload.point_count);
    formData.append('_method', 'patch');
    return axios.post(URL_QUIZZES + payload.id, formData);
}

async function addQuestionAll(payload) {
    const formData = new FormData();
    formData.append('action', 'add-question-all');
    formData.append('bank_id', payload.bank_id);
    formData.append('point_count', payload.point_count);
    formData.append('_method', 'patch');
    return axios.post(URL_QUIZZES + payload.id, formData);
}

async function removeAllQuestion(id) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'remove-all-questions');
    return axios.post(URL_QUIZZES + id, formData);
}

async function getListTest(payload) {
    const params = {
        ...{
            order_by: 'created_at',
            order: 'asc',
            paginated: 0,
            qty: 15,
            page: 1
        },
        ...payload
    }
    return axios.get(URL_QUIZZES, {params: { ...params }})
                .then(response => response.data.data.quizzes);
}

async function createAttempt(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'create-attempt');
    payload.password ? formData.append('password', payload.password) : {};
    return await axios.post(URL_QUIZZES + payload.test_id, formData,     {
        headers: { Accept: 'application/json'}
    });
}

async function createCustomScales(payload) {
    const data = {
        "_method": "patch",
        "action": "create-custom-scales",
        "scales": payload.scales,
    };
    return new Promise((resolve, reject) => {
        axios.post(URL_QUIZZES + payload.id, data, { 'Accept': 'application/json', 'Content-Type': 'application/json' })
                    .then(response => {
                        resolve(response.data.data);
                    })
                    .catch(error => {
                        reject(error.response.data);
                    });
    });
}

async function deleteCustomScales(id) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action','delete-custom-scales');
    return await axios.post(URL_QUIZZES + id, formData, { Accept: 'application/json' });
}

async function updateTestSettings(id, payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    for (var prop in payload) {
        formData.append(prop, payload[prop]);
    }
    return await axios.post(URL_QUIZZES + id, formData)
                        .then(response => {
                            return response.data.data.quiz;
                        });
}

async function updateQuestionRule(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'update-question-rule');
    formData.append('quiz_question_id', payload.question_id);
    formData.append('point_count', payload.point_count);

    return await axios.post(URL_QUIZZES + payload.quiz_id, formData, { 'Accept': 'application/json' });
}

async function removeQuestionRule(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'remove-question-rule');
    formData.append('quiz_question_id', payload.question_id);
    return await axios.post(URL_QUIZZES + payload.quiz_id, formData, { 'Accept': 'application/json' });
}

async function getListInterview(payload) {
    const params = {
        ...{
            order_by: 'created_at',
            order: 'asc',
            paginated: 1,
            qty: 15,
            page: payload.page,
        },
        ...payload
    }
    return axios.get(URL_QUIZZES, {params: { ...params }})
                .then(response => {
                    const noData = ({ data,  ...rest }) => rest;
                    return {
                        data: response.data.data.quizzes.data,
                        paginate: noData(response.data.data.quizzes),
                    }
                });
}

export {
    URL_QUIZZES,
    addQuestionSpecific,
    addQuestionRandom,
    addQuestionAll,
    removeAllQuestion,
    updateQuestionRule,
    removeQuestionRule,
    getListTest,
    getListInterview,
    createAttempt,
    createCustomScales,
    deleteCustomScales,
    updateTestSettings,
}
