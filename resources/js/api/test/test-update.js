// Bashinsky

import axios from '@base/bootstrap';

const URL_QUIZES='/api/v1/quizzes/';
const URL_QUIZES_V2='/api/v2/quizzes/';


async function update(id, payload) {
    const params = {
        '_method': 'patch',
        'action': 'data',
    }
    for (var prop in payload) {
        params[prop] = payload[prop];
    }
    return axios.post(URL_QUIZES + id, params, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then((response) => (response))
        .catch((error) => (error.response));
}


async function update_v2(id, payload) {
    const params = {
        '_method': 'patch',
        'action': 'data',
    }
    for (var prop in payload) {
        params[prop] = payload[prop];
    }
    return axios.post(URL_QUIZES_V2 + id, params, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then((response) => (response))
        .catch((error) => (error.response));
}

async function delete_v2(id){
    const params = {
        '_method': 'delete',
    }
    return axios.post(URL_QUIZES_V2 + id, params).then((response) => (response))
}

export {
    URL_QUIZES,
    update,
    update_v2,
    delete_v2
}
