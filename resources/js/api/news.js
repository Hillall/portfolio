import axios from '@base/bootstrap';

const URL_CATEGORIES = '/api/v1/news-categories';

async function getCategories() {
    try {
        const response = await axios.get(URL_CATEGORIES);
        return response.data.data.categories;
    } catch (e) {
        throw e;
    }
}

export {
    getCategories,
}
