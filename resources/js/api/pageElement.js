// Bashinsky

import axios from '@base/bootstrap';
import { foreachStructure, dataAppend } from '@base/api/helpers/index';

const URL_WELCOME_PAGE_ITEMS = '/api/v1/admin/welcome-page/';

async function addElement(page_id, payload) {
    const formData = new FormData();
        foreachStructure(payload).map((el) =>  {
            const data = dataAppend(el);
            formData.append(data.key, data.value);
        });
    return axios.post(URL_WELCOME_PAGE_ITEMS + page_id + '/items/', formData, { "Accept": "application/json" });
}

async function updateElement(page_id, element_id, payload) {

    const formData = new FormData();

    formData.append('_method', 'PATCH');

    foreachStructure(payload).map((el) =>  {
        const data = dataAppend(el);
        formData.append(data.key, data.value);
    });

    return axios.post(URL_WELCOME_PAGE_ITEMS + page_id + '/items/' + element_id, formData, { "Accept": "application/json" });
}

async function deleteElement(page_id, element_id) {
    return axios.delete(URL_WELCOME_PAGE_ITEMS + page_id + '/items/' + element_id);
}

async function deleteFile(payload) {
    const formData = new FormData();
    formData.append('type', 'BANNERS_DELETE_FILE');
    formData.append('delete_file', payload.file_id);
    formData.append('_method', 'patch');
    return axios.post(URL_WELCOME_PAGE_ITEMS + payload.page_id + '/items/' + payload.element_id, formData, { "Accept": "application/json" });
}

export {
    URL_WELCOME_PAGE_ITEMS,
    addElement,
    updateElement,
    deleteElement,
    deleteFile,
}
