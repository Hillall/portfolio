import axios from '@base/bootstrap';

const URL_COMPETENCE = '/api/v1/competencies/';

async function getCompetencies(payload) {
    const formData = {
        search: payload.search ? payload.search : '',
        only_parents: payload.only_parents ? payload.only_parents : 0,
        only_childs: payload.only_childs ? payload.only_childs : 0,
        order_by: payload.order_by ? payload.order_by : '',
        order: payload.order ? payload.order : '',
        limit: payload.limit ? payload.limit : '',
        qty: payload.qty ? payload.qty : '',
        page: payload.page ? payload.page : '',
        id: payload.id ? payload.id : '',
    };
    if (payload.with) {
        formData['with[]'] = payload.with;
    }
    return axios.get(URL_COMPETENCE, { params: formData })
                .then((response) => response);
}

function createCompetence(payload) {
    const formData = new FormData();
    formData.append('name', payload.name);
    formData.append('description', payload.description);
    if (payload.parent_id) {
        formData.append('parent_id', payload.parent_id);
    }
    if (payload.child_id) {
        formData.append('child_id', payload.child_id ? payload.child_id : '');
    }
    formData.append('type', payload.type ? payload.type : 'competence')
    return axios.post(URL_COMPETENCE, formData)
        .then((response) => response.data.data.competencies);
}

function setRecommendationCompetence(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-recommendation');
    formData.append('competence_id', payload.competence_id);
    formData.append('weight', payload.weight);
    if (payload.weight_reverse) {
        formData.append('weight_reverse', payload.weight_reverse);
    }
    formData.append('duplex', payload.duplex ? payload.duplex : 0);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

function deleteCompetence(id) {
    return axios.delete(URL_COMPETENCE + id);
}

function updateCompetence(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    formData.append('name', payload.name);
    formData.append('type', payload.type ? payload.type : 'competence');
    formData.append('description', payload.description);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

function updateWeightLink(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-weight');
    formData.append('competence_id', payload.competence_id);
    formData.append('weight', payload.weight);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

function unlinkBetweenCompetencies(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'rm-comp-relation');
    formData.append('competence_id', payload.competence_id);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

function unlincRecommendation(payload) {
    const formData = new FormData();
    formData.append('action', 'rm-recommendation');
    formData.append('_method', 'patch');
    formData.append('competence_id', payload.competence_id);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

function linkBeetweenCompetencies(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'add-comp-relation');
    formData.append('competence_id', payload.competence_id);
    formData.append('type_request', 'add-comp-relation');
    formData.append('competence_id', payload.type_request);
    formData.append('weight', payload.weight);
    return axios.post(URL_COMPETENCE + payload.id, formData);
}

export {
    getCompetencies,
    createCompetence,
    deleteCompetence,
    updateCompetence,
    updateWeightLink,
    unlinkBetweenCompetencies,
    linkBeetweenCompetencies,
    unlincRecommendation,
    setRecommendationCompetence,
    URL_COMPETENCE,
};
