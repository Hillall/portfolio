import axios from '@base/bootstrap';
import { foreachStructure, dataAppend } from '@base/api/helpers/index';

const URL_COURSES = '/api/v1/courses/';
const URL_COURSES_V2 = '/api/v2/courses/';

export async function addCourse(payload) {
    const formData = new FormData();
        formData.append('name', payload.name || '');
        formData.append('description', payload.description || '');
        formData.append('organization_id', payload.organization_id);
        formData.append('type', payload.type);

        if (payload.programPreview) {
            formData.append('preview', payload.programPreview);
        }

    return axios.post(URL_COURSES, formData);
}

export async function createDiscipline(payload) {
    const formData = new FormData();
            formData.append('name', payload.name);
            formData.append('parent_id', payload.parent_id);
            formData.append('type', 'discipline');

    return axios.post(URL_COURSES, formData);
}

export async function updateDiscipline(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('name', payload.name);
    formData.append('action', 'data');

    return axios.post(URL_COURSES + payload.id, formData)
}

export async function getCourse(id) {
    return axios.get(URL_COURSES + id);
}

export async function getCourses(payload) {
    const params = {
        parents: payload.parents,
        show: payload.show,
        status: payload.status,
        type: 'course',
        qty: payload.qty,
        page: ((payload.page !== undefined) && (payload.page !== null)) ? Number(payload.page) : 1,
    };
    if (payload.action === 'get-from-organization') {
        params.action = 'get-from-organization';
        params.organization_id = payload.organization_id;
    } else if (payload.action === 'get-from-prog') {
        params.action = 'get-from-prog';
        params.program_id = payload.program_id;
    }

    return axios.get(URL_COURSES, { params });
}

export async function freeSignup(payload){
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'free-signup');

    return await axios.post(URL_COURSES + payload.course_id, formData);
}

export async function deleteDiscipline(id) {
    return axios.delete(URL_COURSES + id);
}

export async function updateCourse(payload) {



    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    formData.append('name', payload.name || '');
    formData.append('description', payload.description || '');
    formData.append('free_enroll', payload.free_enroll ? 1 : 0);
    formData.append('certificate', payload.certificate ? 1 : 0);
    formData.append('couples_count', payload.couples_count);

    foreachStructure(payload.formula).map((el) =>  {
        const data = dataAppend(el);
        formData.append(data.key, data.value);
    });

    let semesters = [];
    if (typeof payload.semesters === 'object') {
        Object.keys(payload.semesters).forEach((prop) => {
            semesters[prop] = payload.semesters[prop];
        });
    } else {
        semesters = payload.semesters;
    }

    semesters.forEach((item, id) => {
        if (item) {
            formData.append(`semesters[${id}]`, item ? 1 : 0);
        }
    });

    if (payload.programPreview) {
        formData.append('preview', payload.programPreview);
    }

    if (payload.indicator_levels) {
        payload.indicator_levels.forEach(item => {
            formData.append('indicator_level[]', item.id);
            formData.append(`can_teaching[${item.id}]`, item.pivot.can_teaching ? 1 : 0);
        })
    }

    if (payload.access_by_user_cards) {
        payload.access_by_user_cards.forEach(card => {
            for(var key in card.filter){
                formData.append('access_by_user_cards['+card.id+']['+key+']', card.filter[key]);
            }
        })
    }


    return await axios.post(URL_COURSES + payload.id, formData);
}


export async function updateCourseV2(payload) {

    return await axios.post(URL_COURSES_V2 + payload.id, payload);
}
