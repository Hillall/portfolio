import axios from '@base/bootstrap';

const URL_LESSON = '/api/v1/lessons/';
const URL_ELEMENT = '/api/v1/elements/';
const URL_ELEMENT_TYPE = '/api/v1/element-types/';

const header = { headers: { 'Content-Type': 'multipart/form-data' } };

function getElementLesson(lessonId, qty, page) {
    return axios.get(URL_ELEMENT, {
        params: {
            lesson_id: lessonId,
            qty,
            page,
        },
    },
    {
        headers: { Accept: 'application/json', 'Content-Type': 'application/x-www-form-urlencoded' },
    })
        .then((response) => response.data.data.elements);
}

function detailElementLesson(id) {
    return axios.get(URL_ELEMENT + id, { headers: { Accept: 'application/json' } })
        .then((response) => response.data.data);
}

function deleteLesson(lesson_id) {
    const formData = new FormData();
    formData.append('_method', 'delete');
    return axios.delete(URL_LESSON + lesson_id, formData)
}

function uploadElementImg({image, lesson_id}) {
    const formData = new FormData();
    formData.append('name', image.name);
    formData.append('type_name', 'image');
    formData.append('image', image);
    formData.append('lesson_id', lesson_id);
    return axios.post(URL_ELEMENT, formData, { Accept: 'application/json' })
        .then((response) => response.data.data);
}

function uploadElementLink(payload) {
    const formData = new FormData();
    const keys = Object.keys(payload);
    keys.forEach(el => formData.append(el, payload[el]));
    formData.append('type_name', 'hyperlink');

    return axios.post(URL_ELEMENT, formData, { Accept: 'application/json' })
        .then((response) => response.data.data);
}

function uploadElementFile({file, lesson_id}) {
    const formData = new FormData();
    formData.append('name', file.name);
    formData.append('type_name', 'file');
    formData.append('file', file);
    formData.append('lesson_id', lesson_id);
    return axios.post(URL_ELEMENT, formData, { Accept: 'application/json' })
        .then((response) => response.data.data);
}

function uploadElementPage(page) {
    const formData = new FormData();
    formData.append('name', page.title);
    formData.append('type_name', 'page');
    formData.append('lesson_id', page.lesson_id);
    formData.append('page[title]', page.title);
    formData.append('page[label]', page.label);
    formData.append('page[content]', page.content);
    formData.append('page[url]', Date.now());
    formData.append('page[meta]', page.meta);
    return axios.post(URL_ELEMENT, formData, { Accept: 'application/json' })
        .then((response) => response.data.data);
}

function uploadElementTest(payload) {
    const formData = new FormData();
    Object.keys(payload).forEach((key) => formData.append(key, payload[key]));
    return axios.post(URL_ELEMENT, formData)
        .then((response) => response.data.data);
}

function deleteElementLesson(id) {
    const formData = new FormData();
    formData.append('_method', 'delete');
    return axios.post(URL_ELEMENT + id, formData, { Accept: 'application/json' });
}

function updateElementLesson(id, name) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    formData.append('name', name);
    return axios.post(URL_ELEMENT + id, formData)
        .then((response) => response.data.data);
}

function updateElementImg(id, image) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-image');
    formData.append('image', image);
    return axios.post(URL_ELEMENT + id, formData)
        .then((response) => response.data.data);
}

function updateElementFail(id, file) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-fail');
    formData.append('fail', file);
    return axios.post(URL_ELEMENT + id, formData)
        .then((response) => response.data.data);
}

function elementTypes() {
    return axios.get(URL_ELEMENT_TYPE,
        { headers: { Accept: 'application/json', 'Content-Type': 'application/x-www-form-urlencoded' } })
        .then((response) => response.data.data.types);
}

async function listLessonWithDiscipline(courseId) {
    return axios.get(URL_LESSON, { params: { course_id: courseId } }).then((res) => res);
}

async function createLesson(payload) {
    const formData = new FormData();
    formData.append('name', payload.name);
    formData.append('couples_count', 1);
    formData.append('course_id', payload.course_id);
    formData.append('description', payload.description);
    return axios.post(URL_LESSON, formData);
}

async function updateLesson(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    formData.append('name', payload.name || '');
    formData.append('description', payload.description || '');
    formData.append('couples_count', payload.couples_count);
    return axios.post(URL_LESSON + payload.id, formData)
}

async function createTask(payload) {
    const formData = new FormData();

    formData.append('name', payload.task.name);
    formData.append('type_name', 'task');
    formData.append('lesson_id', payload.lesson_id);
    formData.append('task[name]', payload.task.name);
    formData.append('task[description]', payload.task.description);

    if (typeof payload.task.documents === 'object') {
        Object.keys(payload.task.documents).forEach((k) => {
            formData.append(`task[documents][${k}]`, payload.task.documents[k]);
        });
    }
    return axios.post(URL_ELEMENT, formData, header);
}

export {
    URL_LESSON,
    getElementLesson,
    detailElementLesson,
    uploadElementImg,
    uploadElementFile,
    uploadElementPage,
    uploadElementTest,
    deleteElementLesson,
    updateElementLesson,
    updateElementImg,
    updateElementFail,
    uploadElementLink,
    elementTypes,
    listLessonWithDiscipline,
    createLesson,
    updateLesson,
    deleteLesson,
    createTask,
};

export default {};
