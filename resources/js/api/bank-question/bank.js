// Bashinsky

import axios from '@base/bootstrap';

const URL_BANKQUESTIONS = '/api/v1/bank-questions/';

async function addBank(payload) {
    const formData = new FormData();
    formData.append('model_id', payload.model_id);
    formData.append('model_type', payload.model_type);
    formData.append('name', payload.name);
    return axios.post(URL_BANKQUESTIONS, formData);
}

async function createBankCategory(payload) {
    const formData = new FormData();
    formData.append('parent_id', payload.parent_id);
    formData.append('name', payload.name);
    return await axios.post(URL_BANKQUESTIONS, formData, { headers: { 'Content-Type': 'application/json' }})
}

async function changeName(payload) {
    const formData = new FormData();
    formData.append('action', 'set-name');
    formData.append('_method', 'patch');
    formData.append('name', payload.name);
    return axios.post(URL_BANKQUESTIONS + payload.id, formData);
}

async function changeParent(payload) {
    const formData = new FormData();
    formData.append('action', 'set-parent');
    formData.append('_method', 'patch');
    formData.append('move_to_bank_id', payload.move_to_bank_id);

    return axios.post(URL_BANKQUESTIONS + payload.selected_bank_id, formData);
}

async function getListBank(payload) {
    return axios.get(URL_BANKQUESTIONS, { params: { ...payload } }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then( response => response.data.data.banks.data );
}

async function deleteCategory(payload) {
    const formData = new FormData();
    formData.append('_method', 'DELETE');
    return await axios.post(URL_BANKQUESTIONS + payload.id, formData, { headers: { 'Content-Type': 'application/json' }});
}

export {
    URL_BANKQUESTIONS,
    addBank,
    createBankCategory,
    changeName,
    changeParent,
    getListBank,
    deleteCategory
}
