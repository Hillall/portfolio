// Bashinsky

import axios from '@base/bootstrap';

const URL_QUESTIONS = '/api/v1/common-questions/';
const URL_QUESTIONS_V2 = '/api/v2/common-questions/';

async function createQuestion(payload) {
    const formData = new FormData();
    const noFileAnswer = ({ type, files, audio, answer_data, params, ...rest }) => rest;

    try {
        for ( let key in noFileAnswer(payload) ) {
            formData.append(key, payload[key]);
        }

        if (payload.params){
            for ( let key in payload.params ) {
                formData.append(`params[${key}]`, payload.params[key]);
            }
        }

        if (payload.files) {
            for ( let key in payload.files ) {
                formData.append(`files[${key}]`, payload.files[key]);
            }
        }

        if (payload.audio) {
            let i = 1;
            for ( let key in payload.audio ) {
                formData.append(`audio[${key}]`, payload.audio[key].blob, 'audio_'+i+'.wav');
                i++;
            }
        }

        if (payload.type === 'relation') {
            for ( let key in payload.answer_data.answers ) {
                for ( let key1 in payload.answer_data.answers[key]) {
                    formData.append(`answer_data[answers][${key}][${key1}]`, payload.answer_data.answers[key][key1]);
                }
            }
        } else {
            for ( let key in payload.answer_data.answers ) {
                formData.append(`answer_data[answers][${key}]`, payload.answer_data.answers[key]);
            }
        }

        if (typeof payload.answer_data['correct'] !== "undefined") {
            if (typeof payload.answer_data['correct'] === 'object') {
                for ( let key in payload.answer_data.correct ) {
                    formData.append(`answer_data[correct][${key}]`, payload.answer_data.correct[key]);
                }
            } else {
                formData.append(`answer_data[correct]`, payload.answer_data.correct);
            }
        }
    } catch (e) {
        console.warn(e)
    }



    return axios.post(URL_QUESTIONS_V2, formData, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function updateQuestion(payload) {
    payload._method = 'PATCH';
    payload.action = 'full_update';
    return axios.post(URL_QUESTIONS + payload.id, { ...payload }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function deleteAnswer(payload){
    payload._method = 'PATCH';
    payload.action = 'delete_question_answers';

    return axios.post(URL_QUESTIONS + payload.id, { ...payload }, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

async function getListQuestions(payload) {
    return axios.get(URL_QUESTIONS, { params: { ...payload } })
                .then(response => response.data.data)
}

async function getQuestion(id) {
    return axios.get(URL_QUESTIONS + id)
}

async function delQuestion(id) {
    const formData = new FormData();
    formData.append('_method', 'DELETE');
    return axios.post(URL_QUESTIONS + id,  formData,  { headers: { 'Accept': 'application/json', }});
}

async function deleteMedia(payload) {
    const formData = new FormData();
    formData.append('action', 'detach-media');
    formData.append('_method', 'patch');
    formData.append('media_id', payload.media_id);
    try {
        const response = await axios.post(URL_QUESTIONS + payload.question_id, formData);
        return response.data.data;
    } catch (e) {
        throw e;
    }
}

async function updateMedia(payload) {
    const formData = new FormData();
    formData.append('action', 'attach-medias');
    formData.append('_method', 'patch');
    formData.append('collection', payload.type);
    if (payload.type === 'audio') {
        for (let key in payload.media) {
            formData.append(`media[${key}]`, payload.media[key].blob)
        }
    }
    if (payload.type === 'files') {
        for (let key in payload.media) {
            formData.append(`media[${key}]`, payload.media[key])
        }
    }

    try {
        const response = await axios.post(URL_QUESTIONS_V2 + payload.question_id, formData);
        return response.data.data;
    } catch (e) {
        return e;
    }
}

export {
    URL_QUESTIONS,
    getQuestion,
    getListQuestions,
    createQuestion,
    updateQuestion,
    delQuestion,
    deleteMedia,
    updateMedia,
    deleteAnswer
};
