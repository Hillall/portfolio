// Bashinsky
const foreachStructure = (structure) => {
    const data = [];
    for ( let keyStructure in structure ) {
        if (structure[keyStructure] instanceof File) {
            data.push([keyStructure,  structure[keyStructure]]);
        } else if (typeof structure[keyStructure] === 'object') {
            for (let keyValue in structure[keyStructure]) {
                if (structure[keyStructure][keyValue] instanceof File) {
                    data.push([keyStructure, keyValue,  structure[keyStructure][keyValue]]);
                } else if (typeof structure[keyStructure][keyValue] === 'object') {
                    let result = foreachStructure(structure[keyStructure][keyValue]);
                    result.map(el => {
                        if ( Array.isArray(el) ) {
                            data.push([keyStructure, keyValue,  ...el]);
                        } else {
                            data.push([keyStructure, keyValue,  el]);
                        }
                    })
                } else {
                    data.push([keyStructure, keyValue, structure[keyStructure][keyValue]]);
                }
            }
        } else {
            data.push([keyStructure, structure[keyStructure]]);
        }
    }
    return data;
}

const dataAppend = (el) => {
    return {
        key: el.reduce( (acc, curr, index) => {
            if (index === 0)  {
                return curr;
            }  else if (index === el.length - 1) {
                return acc;
            }  else {
                return acc + `[${curr}]`;
            }
        } ),
        value: el[el.length - 1] }
}

export {
    dataAppend,
    foreachStructure,
}
