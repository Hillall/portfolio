import axios from '@base/bootstrap';

const URL_GROUP = '/api/v1/groups/';

async function getListGroup(payload) {
    /* const formData = {
        action: payload.action,
        program_id: payload.program_id,
        user_id: payload.program_id,
        order_by: payload.order_by,
        order: payload.order,
        qty: payload.qty,
        page: payload.page,
        show: payload.show,
    }; */
    return axios.get(URL_GROUP, { params: payload })
                .then((response) => response.data.data)
}

async function getGroupDetail(id) {
    return axios.get(URL_GROUP + id)
                .then((response) => response);
}

async function createGroup(payload) {
    let data = JSON.stringify({
        program_id: payload.program_id,
        name: payload.name,
        min: payload.min,
        max: payload.max,
        started_at: payload.started_at,
        finished_at: payload.finished_at,
        date_check_filling: payload.date_check_filling,
        show: payload.show,
        schedules: payload.schedules,
    });
    return axios.post(URL_GROUP, data, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => response);
}


async function deleteGroup(id) {
    const formData = new FormData();
    formData.append('_method', 'delete');
    return axios.delete(URL_GROUP + id, formData)
                .then((response) => response);
}

async function signoutGroup(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'signout');
    formData.append('user_id', payload.user_id);
    return axios.post(URL_GROUP + payload.id, formData)
                .then((response) => response);
}

async function signupGroup(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'signup');
    formData.append('user_id', payload.user_id);
    return axios.post(URL_GROUP + payload.id, formData);
                // .then((response) => response);
}

async function movetoGroup(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'moveto');
    formData.append('user_id', payload.user_id);
    formData.append('course_id', payload.course_id);
    return axios.post(URL_GROUP + payload.id, formData);
}

async function updateGroup(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'data');
    for (var prop in payload) {
        formData.append(prop, payload[prop]);
    }

    return axios.post(URL_GROUP + payload.id, formData).then((response) => response);
}
async function updateGroupVisible(payload) {
    const formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'set-show');
    for (var prop in payload) {
        formData.append(prop, payload[prop]);
    }
    return axios.post(URL_GROUP + payload.id, formData);
}

export {
    URL_GROUP,
    getListGroup,
    getGroupDetail,
    createGroup,
    deleteGroup,
    signoutGroup,
    signupGroup,
    movetoGroup,
    updateGroup,
    updateGroupVisible
};
