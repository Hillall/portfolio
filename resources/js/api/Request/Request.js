class Request {
    constructor(formData) {
        this.formData = formData;
    }

    appendAll(payload) {
        for(let k in payload){
            if (payload[k]){
                this.formData.append(k, payload[k]);
            }
        }
        return this;
    }

    patch() {
        this.formData.append('_method', 'PATCH');
        return this;
    }
}

export default Request;
