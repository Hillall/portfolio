import axios from '@base/bootstrap';

const URL_PROGRAMS = '/api/v1/programs/';

async function getPrograms(payload) {
    try {
        const response = await axios.get(URL_PROGRAMS, { params: payload });
        return response.data.data;
    } catch (e) {
        throw e;
    }

}

export {
    getPrograms,
}
