// Bashinsky

import axios from '@base/bootstrap';

const URL_ADMIN_WELCOME_PAGE = '/api/v1/admin/welcome-pages/';
const URL_WELCOME_PAGE = '/api/v1/welcome-pages/';
const URL_WELCOME_PAGE_MAIN = '/api/v1/welcome-page/main';

async function getPages(payload) {
    return axios.get(URL_ADMIN_WELCOME_PAGE, { params: payload }, { Accept: 'application/json' });
}

async function getPage(page_id) {
    try {
        const response = await axios.get(URL_WELCOME_PAGE + page_id);
        return response.data.data;
    } catch (e) {
        throw e;
    }

}

async function createPage(payload) {
    const formData = new FormData();
    for(let k in payload){
        if (payload[k]){
            formData.append(k, payload[k]);
        }
    }
    try {
        const response = await axios.post(URL_ADMIN_WELCOME_PAGE, formData);
        return response.data;
    } catch (e) {
        throw e;
    }
}

async function updatePage(payload) {
    const noId = ({ id, ...rest }) => rest;
    const formData = new FormData();
    for(let k in noId(payload)){
        if (payload[k]){
            formData.append(k, payload[k]);
        }
    }
    formData.append('_method', 'PATCH');
    try {
        const response = await axios.post(URL_ADMIN_WELCOME_PAGE + payload.id, formData);
        return response.data.data;
    } catch (e) {
        throw e;
    }
}

async function getMainPage() {
    try {
        const response = await axios.get(URL_WELCOME_PAGE_MAIN, {},  { Accept: 'application/json' });
        // console.log( response.data.data);
        return response.data.data;
    } catch (e) {
        throw e;
    }
}

async function deletePage(page_id) {
    try {
        await axios.delete(URL_ADMIN_WELCOME_PAGE + page_id);
    } catch (e) {
        throw e;
    }
}

export {
    URL_ADMIN_WELCOME_PAGE,
    getPages,
    getPage,
    createPage,
    getMainPage,
    updatePage,
    deletePage,
}
