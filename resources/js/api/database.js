import axios from '@base/bootstrap';

const URL_DATABASE = '/api/v1/databases/';

async function getTableDetails(payload) {
    return axios.get(URL_DATABASE + payload.id, { params: { ...payload }})
        .then((response) => response.data.data.database);
}

async function getTables(payload) {
    return axios.get(URL_DATABASE, { params: { ...payload }})
        .then(response => response.data.data.databases);
}

export {
    URL_DATABASE,
    getTableDetails,
    getTables,
}
