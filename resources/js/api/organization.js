// Bashinsky

import axios from '@base/bootstrap';

const URL_ORGANIZATION = '/api/v1/organizations/';


function getOrganizations(data){
    return axios.get(URL_ORGANIZATION, {params: data})
        .then((response) => response);
}

async function getOrganization(id) {
    return await axios.get(URL_ORGANIZATION + id);
}


async function createOrganization(data){
    return axios.post(URL_ORGANIZATION, data)
        .then((response) => response)
        .catch((error) => error.response);
}

async function getOrganizationVacancies(data, id) {
    return axios.get(URL_ORGANIZATION + id + '/vacancies', {
        params: data
    })
        .then((response) => response.data)
        .catch((error) => error.response);
}

async function getOrganizationVacancy(id, vacancy) {
    return axios.get(URL_ORGANIZATION + id + '/vacancies/' + vacancy)
        .then((response) => response.data)
        .catch((error) => error.response);
}

async function createOrganizationVacancy(id, data) {
    let formData = new FormData();
    for(var k in data){
        if (data[k]){
            formData.append(k, data[k]);
        }
    }
    return axios.post(URL_ORGANIZATION + id + '/vacancies', formData)
        .then((response) => response)
        .catch((error) => error.response);
}

async function updateOrganizationVacancy(org_id, data) {
    let formData = new FormData();
    for(var k in data){
        if (data[k]){
            formData.append(k, data[k]);
        }
    }
    formData.append('_method', 'patch');
    return axios.post(URL_ORGANIZATION + org_id + '/vacancies/' + data.id, formData)
        .then((response) => response)
        .catch((error) => error.response);
}

async function addVacancyEducationProgram(org_id, vacancy_id, education_program_id) {
    let formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'add_education_program');
    formData.append('education_program_id', education_program_id);

    return axios.post(URL_ORGANIZATION + org_id + '/vacancies/' + vacancy_id, formData)
        .then((response) => response)
        .catch((error) => error.response);
}

async function deleteVacancyEducationProgram(org_id, vacancy_id, education_program_id) {
    let formData = new FormData();
    formData.append('_method', 'patch');
    formData.append('action', 'delete_education_program');
    formData.append('education_program_id', education_program_id);

    return axios.post(URL_ORGANIZATION + org_id + '/vacancies/' + vacancy_id, formData)
        .then((response) => response)
        .catch((error) => error.response);
}


async function createVacancyResponse(org_id, vacancy_id) {
    let formData = new FormData();
    return axios.post(URL_ORGANIZATION + org_id + '/vacancy/' + vacancy_id + '/vacancy_response', formData)
        .then((response) => response)
        .catch((error) => error.response);
}

async function getOrganizationVacanciesResponses(id, data) {
    return axios.get(URL_ORGANIZATION + id + '/vacancyResponses')
        .then((response) => response)
        .catch((error) => error.response);
}

async function saveVacancyResponseStatus(org_id, item) {
    let formData = new FormData();
    formData.append('status', item.status);
    formData.append('_method', 'patch');

    return axios.post(URL_ORGANIZATION + org_id + '/vacancy/' + item.vacancy_id + '/vacancy_response/' + item.id, formData)
        .then((response) => response)
        .catch((error) => error.response);
}

async function getOrganizationQuizzes(data) {
    return axios.get('/api/v1/quizzes/', {params: data})
        .then((response) => response)
        .catch((error) => error.response);
}

async function getOrganizationResultQuizzes(data) {
    return axios.get('/api/v2/quizzes_result/', {params: data})
        .then((response) => response)
        .catch((error) => error.response);
}

async function createOrganizationTest(org_id, data) {
     return axios.post(URL_ORGANIZATION + org_id, data)
        .then((response) => response)
}


async function createInterview(organization_id, payload) {
    let data = {
        "_method": "patch",
        action: "create-interview",
        start: payload.start,
        end: payload.end,
        lead_time: payload.lead_time,
        name: payload.name,
        params: {
            rule: payload.rule,
            change_answers: payload.change_answers,
        }
    }
    data.params.rule === 'users-with-role' ? data.params.role_id = payload.role_id : {};
    return axios.post(URL_ORGANIZATION + organization_id, data, { "Accept": "application/json", "Content-Type":  "application/json" });
}

export {
    URL_ORGANIZATION,
    getOrganizations,
    createOrganization,
    getOrganizationVacancies,
    getOrganizationVacancy,
    createOrganizationVacancy,
    updateOrganizationVacancy,
    createVacancyResponse,
    getOrganizationVacanciesResponses,
    saveVacancyResponseStatus,
    addVacancyEducationProgram,
    deleteVacancyEducationProgram,
    getOrganizationQuizzes,
    getOrganizationResultQuizzes,
    createOrganizationTest,
    createInterview,
    getOrganization,
}
