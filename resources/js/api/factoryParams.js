export class factoryParams {

    constructor() {
        this.params = {}
    }

    with(payload) {
        this.params.with = payload;
        return this;
    }

    paginate(payload) {
        this.params = {
            ...this.params,
            qty: 5, // payload.per_page,
            page: payload.current_page,
            paginated: 1,
        }

        return this;
    }

    filter(filter) {
        if (filter) {
            this.params[filter] = 1;
        }
        return this;
    }

    order(payload) {
        this.params = {
            ...this.params,
            order_by: payload.order_by,
            order: payload.order,
        }

        return this;
    }

    getParams() {
        return this.params;
    }

    search(search) {
        if (search) {
            this.params.search = search;
        }
        return this;
    }

    search_p(param, value){
        if (value){
            this.params[param] = value
        }
        return this;
    }
}
