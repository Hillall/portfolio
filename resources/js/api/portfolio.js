// Bashinsky

import axios from '@base/bootstrap';

const URL_PORTFOLIO = '/api/v1/portfolio/';
const URL_PORTFOLIO_ITEM = '/api/v1/portfolio_item/';

async function loadPortfolio(data){
    return axios.get(URL_PORTFOLIO + data.id, {
        params: data
    }).then((response) => response.data).catch((error) => error.response);
}

async function loadMyPortfolio(data){
    return axios.get(URL_PORTFOLIO, {
        params: data
    }).then((response) => response.data);
}

async function updatePortfolio(data, id){
    data._method = 'PATCH';
    return axios.post(URL_PORTFOLIO + id, data, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function sendPortfolioRequest(id){
    const formData = new FormData();
    return axios.post(URL_PORTFOLIO + id + '/send_request', formData, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function loadPortfolioRequests(data){
    return axios.get(URL_PORTFOLIO + 'my/requests', {
        params: data
    }, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function changeRequestsStatus(data, id){
    return axios.post(URL_PORTFOLIO + 'my/request/' + id , data, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function updateItem(data, item_id) {
    data.append('_method', 'PATCH');
    return axios.post(URL_PORTFOLIO_ITEM + item_id, data, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function addItem(data) {
    return axios.post(URL_PORTFOLIO_ITEM, data, { Accept: 'application/json' })
        .then((response) => response.data);
}

async function deleteItem(data) {
    const formData = new FormData();
    formData.append('_method', 'delete');

    return axios.post(URL_PORTFOLIO_ITEM+data.id, formData, { Accept: 'application/json' })
        .then((response) => response.data);
}


export {
    URL_PORTFOLIO,
    URL_PORTFOLIO_ITEM,
    updatePortfolio,
    loadPortfolio,
    loadPortfolioRequests,
    changeRequestsStatus,
    sendPortfolioRequest,
    loadMyPortfolio,
    addItem,
    updateItem,
    deleteItem
}
