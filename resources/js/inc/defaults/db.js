export const defStruct = {
    id: 0,
    name: '',
    type: 'PUBLIC',
    status: 'DRAFT',
    organization_id: 0,
    cols: [],
    organizations: [],
    struct_categories: [],
    created_by_organization: [],
};

export const defStructCategory = {
    id: 0,
    name: '',
    tag: '',
    parent_id: null,
    organization_id: 0,
    show: 0,
    popularity: 0,
    type: 'PRIVATE',
    database_structs: [],
    children_with_structs: [],
    created_at: '',
    updated_at: '',
};

export const defTable = {
    id: 0,
    name: '',
    model_type: '',
    model_id: 0,
    model: {},
    created_at: '',
    updated_at: '',
    private: 0,
    priority: 0,
    database_struct_id: 0,
    struct_category_id: 0,
    organization_id: 0,
    database_struct: {},
    organization: {},
};

export const defCol = {
    id: 0,
    name: '',
    order: 2,
    database_col_type_id: 6,
    database_struct_id: 1,
    properties: {},
    type: {},
    updated_at: '',
    created_at: '',
};

export const defColType = {
    id: 0,
    name: '',
    system_name: '',
    properties: {},
    updated_at: '',
    created_at: '',
};

export const defFile = {
    id: 0,
    order: 0,
    name: '',
    properties: [],
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    laravel_through_key: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'File',
        properties: [],
        created_at: '',
        updated_at: '',
    },
};

export const defSelect = {
    id: 0,
    order: 0,
    name: '',
    properties: {
        type_id: '',
        select: {},
    },
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    laravel_through_key: 0,
    type: {
        id: 1,
        name: '',
        system_name: 'Select',
        properties: {
            types: {
                inner: [],
                external: [],
            },
        },
        created_at: '',
        updated_at: '',
    },
};

export const defText = {
    id: 0,
    order: 0,
    name: '',
    properties: {
        type_id: '',
    },
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    laravel_through_key: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Text',
        properties: {
            types: {
                text: {
                    name: 'Большой текст',
                    'max-size': 65000,
                },
                string: {
                    name: 'Строка',
                    'max-size': 255,
                },
            },
        },
        created_at: '',
        updated_at: '',
    },
};

export const defDatetime = {
    id: 0,
    order: 0,
    name: '',
    properties: {
        type_id: '',
    },
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    laravel_through_key: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Datetime',
        properties: {
            types: {
                date: {
                    name: 'Дата',
                    format: 'Y-m-d',
                },
                time: {
                    name: 'Время',
                    format: 'H:i:s',
                },
                datetime: {
                    name: 'Дата и время',
                    format: 'Y-m-d H:i:s',
                },
            },
        },
        created_at: '',
        updated_at: '',
    },
};

export const defCheckbox = {
    id: 0,
    order: 0,
    name: '',
    properties: [],
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Checkbox',
        properties: [],
        created_at: '',
        updated_at: '',
    },
};

export const defHiperlink = {
    id: 0,
    order: 0,
    name: '',
    properties: [],
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Hyperlink',
        properties: [],
        created_at: '',
        updated_at: '',
    },
};

export const defInteger = {
    id: 0,
    order: 0,
    name: '',
    properties: [],
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Integer',
        properties: [],
        created_at: '',
        updated_at: '',
    },
};

export const defDouble = {
    id: 0,
    order: 0,
    name: '',
    properties: [],
    database_col_type_id: 0,
    created_at: '',
    updated_at: '',
    database_struct_id: 0,
    type: {
        id: 0,
        name: '',
        system_name: 'Double',
        properties: [],
        created_at: '',
        updated_at: '',
    },
};

export const defRow = {
    id: 0,
    order: 0,
    database_id: 0,
    values: [],
    created_at: '',
    updated_at: '',
};

export const defRowValue = {
    id: 0,
    value: '',
    database_col_id: 0,
    database_row_id: 0,
    created_at: '',
    updated_at: '',
    type_col: {},
    col: {},
};

const defData = {};

export default defData;
