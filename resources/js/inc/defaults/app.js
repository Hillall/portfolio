// Bashinsky

export const defOrganization = {
    address: '',
    created_at: '',
    description: '',
    id: 0,
    logo: '',
    map_coordinates_x: 0,
    map_coordinates_y: 0,
    name: '',
    email: '',
    phone: '',
    popularity: 0,
    show: 0,
    type: '',
    updated_at: '',
};

export const defNews = {
    author_id: 0,
    category_id: 0,
    description: '',
    id: 0,
    image_original: '',
    name: '',
    organization_id: 0,
    status: '',
    text: '',
    title: '',
    publish_at: '',
    updated_at: '',
    created_at: '',
    image_conversions: {},

    meta: [],
    author: {},
    category: {},
    organization: defOrganization,
};

export const defUser = {
    avatar: {},
    email: '',
    esia_id: null,
    firstname: '',
    id: 0,
    lastname: '',
    login: '',
    patronymic: null,
    phone: null,
    updated_at: '',
    created_at: '',
    competencies: [],
    organizations: [],
};

export const defPage = {
    id: 0,
    url: '',
    label: '',
    title: '',
    content: '',
    meta: '',
    documents: [],
    show: 0,
};

export const defDepartment = {
    id: 0,
    name: '',
    show: true,
    organization_id: 0,
    parent_id: 0,
    created_at: '',
    updated_at: '',
    display_in_show: true,
    childs: [],
};

export const defDepartmentContact = {
    id: 0,
    email: '',
    phone: '',
    position: '',
    order: 1,
    show: true,
    user_id: 0,
    organization_department_id: 0,
    created_at: '',
    updated_at: '',
    display_in_show: false,
};

export const defProgram = {
    name: '',
    description: '',
    organization_id: 0,
    popularity: 0,
    show: false,
    main_popularity: 0,
    main_show: 0,
};

export const defCourse = {
    created_at: '',
    id: 0,
    name: '',
    organization_id: 0,
    parent_id: null,
    popularity: 0,
    preview: '',
    show: false,
    status: '',
    type: '',
    updated_at: '',
};

export const defLesson = {
    id: 0,
    name: '',
    course_id: 0,
    couples_count: 0,
    order: 1,
    created_at: '',
    updated_at: '',
};

export const defLessonTask = {
    id: 0,
    name: '',
    description: '',
    author_id: 0,
    editor_id: 0,
    model_type: '',
    model_id: 0,
    created_at: '',
    updated_at: '',
    documents: [],
    media: [],
};

export const defRole = {
    id: 0,
    name: '',
    label: '',
    show: 0,
    position: 0,
    created_at: '',
    updated_at: '',
};

export const defPermission = {
    id: 0,
    name: '',
    label: '',
    show: 0,
    position: 0,
    created_at: '',
    updated_at: '',
};

export const defTestSettings = {
    change_answers: false,
    count_attempts: 0,
    see_result_questions: false,
    passing_score: 0,
    start_time_availability: new Date(),
    end_time_availability: new Date(),
    lead_time: 0,
    name: '',
    protected: false,
}

export const defBlock = {
    type: '',
    structure: {},
    show: false,
    position: 1,
};

export const defSliderStructure = {
    text: "",
    image: ""
};

export const defMenuStructure = {
    text: "",
    url: ""
};

export const defNewsStructure = {
    category: 0,
    count: 0,
};

export const defProgramStructure = {
    id: 0,
};

export const defBannerStructure = {
    size: '',
    background: '#452C4582',
    html: '',
    type: 'banner',
};

export const defBannerMenuStructure = {
    size: '',
    background: '#452C4582',
    type: 'menu',
    items: [],
};

export const defLinkStructure = {
    title: '',
    url: '',
};

export const defHtmlStructure = {
    html: '',
};

export const defHeadStructure = {
    image: '',
    text: '',
}

export const defWelcomePage = {
    name: '',
    title: '',
    description: '',
    type: '',
    status: 'DRAFT',
};

const defData = {};

export default defData;
