// Bashinsky

function customDataFormat(date) {
    const noTimezone = noTimezoneOffset(new Date(date));
    // console.log('data', newDate.toISOString());
    return noTimezone.toISOString().substring(0, 16);
}

function noTimezoneOffset(date) {
    return new Date(date.getTime() - date.getTimezoneOffset() * 60000)
}

function dateToSeconds(date) {
    return date / 1000;
}

function difference(start, end) {
    return end - start;
}

function toMinutes(date) {
    const total = Math.floor( date / 60);
    return total;
}

function toSeconds(date) {
    const total = Math.trunc(date % 60);
    return total;
}

function totalMinSec(start, end) {
    const min = toMinutes(dateToSeconds(difference(start, end)));
    const sec = toSeconds(dateToSeconds(difference(start, end)));

    return {
        min: isNaN(min) ? 0 : min,
        sec: isNaN(sec) ? 0 : sec,
    };
}



export default {
    customDataFormat,
    dateToSeconds,
    difference,
    toMinutes,
    toSeconds,
    totalMinSec,
};

