class Paginate {
    static calculate(data) {
        const pagination = {
            page: {},
            page_current: 0,
            prev: { show: false, page: 0 },
            next: { show: false, page: 0 },
        };

        let n = 0;
        let start = 0;
        let end = 0;
        const range = 4;

        if (data.current_page - range > 0) {
            start = data.current_page - range;
        } else {
            start = 1;
        }

        if (data.current_page + range <= data.last_page) {
            end = data.current_page + range;
        } else {
            end = data.last_page;
        }

        if (data.current_page > 1) {
            pagination.prev.show = true;
            pagination.prev.page = data.current_page - 1;
        }
        if (data.current_page < data.last_page) {
            pagination.next.show = true;
            pagination.next.page = data.current_page + 1;
        }

        for (let i = start; i <= end; i += 1) {
            pagination.page[n] = i;
            n += 1;
        }

        pagination.page_current = data.current_page;

        return pagination;
    }
}

export default Paginate;
