
import Vue from 'vue';
import Vuex from 'vuex';
import axios from '../bootstrap';
import courses from './modules/courses/index';
import programs from './modules/programs/index';
import lessons from './modules/lessons/index';
import lessonTask from './modules/lessons/task';
import organization from './modules/organization/index';
import user from './modules/user/index';
import test from './modules/test/index';
import bank from './modules/bank/index';
import interview from './modules/interview/index';
import basket from './modules/basket/index';
import iom from './modules/iom/index';
import routes from './modules/routes/index';
import welcomepage from './modules/welcomepage/index';
Vue.use(Vuex);

const getOutOfRoles = (roles) => {
    const rolesPermissions = roles.map(el => el.permissions);
    let permissions = rolesPermissions.reduce( (acc, el) => ([ ...acc, ...el ]));
    permissions = permissions.map((el) => ({ [el.name]: true })).reduce((acc, el) => ({ ...acc, ...el }));
    return permissions;
}

const store = new Vuex.Store({
    state: {
        auth: false,
        internetError: false,
        popup: false,
        roles: [],
        competencies: {},
        main_page: {},
        organizations: [],
        edu_organizations: {},
        online: {
            count: 0,
            users: {

            }
        },
        loading: {
            organization: '',
            competence: '',
            auth: '',
            local: 0,
            global: 0,
        },
        update: {
            org: {
                news: [],
                about: [],
                teacher: [],
                department: [],
                schedule: [],
                lectureRoom: [],
                program: [],
                course: [],
                module: [],
                lesson: [],
            },
            news: [],
        },
        globalPermissions: {
            iom_municipal_tutoring: false,
            iom_regional_tutoring: false
        },
    },
    mutations: {
        auth(state, { data }) {
            state.auth = data; // eslint-disable-line no-param-reassign
        },
        internetError(state, data){
            state['internetError'] = true;

        },
        setStore(state, { key, data, allowCreate = false }) {
            if (key.includes('.')) {
                let tmp = state;
                const arrKey = key.split('.');
                for (const i in arrKey) { // eslint-disable-line guard-for-in, no-restricted-syntax
                    const keyInStore = arrKey[i];
                    if (typeof tmp[keyInStore] !== 'undefined') {
                        if (keyInStore === arrKey[arrKey.length - 1]) {
                            tmp[keyInStore] = data;
                        }
                        tmp = tmp[keyInStore];
                    } else if (allowCreate) {
                        if (keyInStore === arrKey[arrKey.length - 1]) {
                            tmp[keyInStore] = data;
                        }
                        tmp = tmp[keyInStore];
                    }
                }
            } else {
                state[key] = data; // eslint-disable-line no-param-reassign
            }
        },
    },
    getters: {
        auth: (state) => state.auth,
        main_page: (state) => state.main_page,
        getOnline: (state) => state.online,
        internetError: (state) => state.internetError,
        authRoles: (state) => state.roles,
        authCompetencies: (state) => state.competencies,
        authOrganizations: (state) => state.organizations,
        authEduOrganizations: (state) => state.edu_organizations,
        getStore: (state) => (key) => {
            if (key.includes('.')) {
                let tmp = state;
                const arrKey = key.split('.');
                for (const i in arrKey) { // eslint-disable-line guard-for-in, no-restricted-syntax
                    const keyInStore = arrKey[i];
                    if (typeof tmp[keyInStore] !== 'undefined') {
                        if (keyInStore === arrKey[arrKey.length - 1]) {
                            return tmp[keyInStore];
                        }
                        tmp = tmp[keyInStore];
                    }
                }
                return undefined;
            } else { // eslint-disable-line no-else-return
                return state[key];
            }
        },
    },
    actions: {

        /*
        actionName({state, dispatch, commit}, params) {
            dispatch(actionName, params);
            commit(mutationName, params);
        },
        */
        clearAuth({ commit, state }) {
            commit('setStore', { key: 'auth', data: false });
            commit('setStore', { key: 'roles', data: [] });
            commit('setStore', { key: 'organizations', data: {} });
            commit('setStore', { key: 'edu_organizations', data: {} });
            commit('setStore', { key: 'competencies', data: {} });
            commit('setStore', { key: 'main_page', data: {} });
            state.globalPermissions = {};
        },
        async getAuth({ dispatch, commit, state }) { // eslint-disable-line

            await axios.get('/api/v1/me', {})
                .then(({
                    data: {
                        auth, roles, edu_organizations,organizations, main_page
                    },
                }) => {

                    commit('setStore', { key: 'auth', data: auth });
                    commit('setStore', { key: 'roles', data: roles });
                    commit('setStore', { key: 'organizations', data: organizations });
                    commit('setStore', { key: 'edu_organizations', data: edu_organizations });
                    commit('setStore', { key: 'main_page', data: main_page });
                    const globalPermission = getOutOfRoles(auth.roles);
                    state.globalPermissions = globalPermission;
                    // commit('setStore', { key: 'permissions', data: response.data.permissions });

                })
                .catch((error) => {

                })
                .finally(() => {});
        },
        getCompetence({ dispatch, commit }, { loading }) {

            axios.get('/api/v1/me/competencies', {})
                .then(({ data: { competencies } }) => {
                    commit('setStore', { key: 'competencies', data: competencies });

                })
                .catch((error) => {

                })
                .finally(() => {});
        },
        getOrganization({ dispatch, commit }, { loading }) {

            axios.get('/api/v1/me/organization', {})
                .then(({ data: { organizations } }) => {
                    commit('setStore', { key: 'organizations', data: organizations });

                })
                .catch((error) => {

                })
                .finally(() => {});
        },
        animateLoading({ getters, commit }, { type, action, status_code }) {
            let value = getters.getStore(`loading.${type}`);
            switch (action) {
                case 'start':
                    commit('setStore', { key: 'internetError', data: false });
                    value += 1;
                    break;
                case 'finish':
                    value -= 1;
                    break;
                case 'error':
                    if (status_code == 0){
                        commit('setStore', { key: 'internetError', data: true });
                    }
                    value -= 1;
                    break;
                default:
                    break;
            }
            if (value < 0) {
                value = 0;
            }
            commit('setStore', { key: `loading.${type}`, data: value });
        }
    },
    modules: {
        courses,
        programs,
        lessons,
        lessonTask,
        organization,
        user,
        test,
        bank,
        interview,
        basket,
        iom,
        routes,
        welcomepage,
    },
});

export default store;
