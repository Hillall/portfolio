import { getListInterview } from '@base/api/test/adding-questions';
import { createInterview } from '@base/api/organization';
const data = {
    interview: { 
        data: [],
        paginate: {}
    },
};


const getters = {
};

const mutations = {

};

const actions = {
    async GET_BANK({ state }, payload) {
        try {
            const bank = await getListBank({ organization_id: payload.organization_id, 'with[]': 'tree-childs'});
            state.bank = bank[0].tree_childs[0];
            const noChilds = ({ childs, tree_childs,  ...rest }) => rest;
            state.category = noChilds(bank[0]);
        }
        catch(e) {
            throw e;
        }
    },
    async GET_INTERVIEWS({state}, payload) {
        try {
            const interview = await getListInterview({  type: 'interview', 
                                                   organization_id: payload.organization_id,
                                                   page: payload.page });
            state.interview = interview;
        }
        catch (e) {
            throw e;
        }
    },
    async CREATE_INTERVIEW({state, rootState }, payload) {
        try {
            const interview = await createInterview(rootState.organization.organization.id, payload);
            state.interview.data.push(interview.data.data.interview);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};