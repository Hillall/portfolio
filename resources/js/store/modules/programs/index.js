import axios from '@base/bootstrap';

const data = {
    program: null,
    program_selected: true,
    speciality_types: null,
    invalid: {},
    spinner: { save: false },
    teachers: [],
    groups: [],
};

const getters = {
    MODAL: (state, getters, rootState, rootGetters) => rootGetters.getStore('modal.org.addProgram'), // eslint-disable-line
    PROGRAM: (state) => state.program,
    SPECIALITY_TYPES: (state) => state.speciality_types,
    PROGRAM_SELECTED: (state) => state.program_selected,
    INVALID: (state) => state.invalid,
    SPINNER: (state) => state.spinner,
    TEACHERS: (state) => state.teachers,
    GROUPS: (state) => state.groups,
};

const mutations = {
    SET_PROGRAM: (state, payload) => {
        state.program = payload; // eslint-disable-line
    },
    SET_SPECIALITY_TYPES: (state, payload) => {
        state.speciality_types = payload;
    },
    SET_PROGRAM_SELECTED: (state, payload) => {
        state.program_selected = payload; // eslint-disable-line
    },
    SET_TEACHERS: (state, payload) => {
        state.teachers = payload; // eslint-disable-line
    },
    SET_INVALID: (state, payload) => {
        state.invalid = payload; // eslint-disable-line
    },
    SET_SPINNER: (state, payload) => {
        state.spinner.save = payload // eslint-disable-line
    },
    ADD_TEACHER: (state, payload) => {
        state.teachers.push(payload);
    },
    SET_GROUPS: (state, payload) => {
        state.groups = payload; // eslint-disable-line
    },
    REMOVE_GROUP: (state, payload) => {
        state.groups.data = state.groups.data.filter(el => el.id !== payload);
    },
    SET_GROUP_USERS: (state, payload) => {
        state.groups.data = state.groups.data.map(group => {
            if (group.id === payload.id) {
                group.users = payload.users;
            }
            return group;
        });
    }
};

const actions = {
    ADD_SPECIALITY_TYPES(context, payload){

        const params = {
            'education_program': payload.program_id,
            'education_program_speciality_type': payload.type_id
        };
         return axios.post('/api/v1/programs/'+payload.program_id+'/add_speciality_type', params )
            .then(({ data: { data: { program } } }) => {

                return program;
            })
            .catch(() => {

            });
    },
    async GET_SPECIALITY_TYPES(context, payload){

        const params = {

        };
        await axios.get('/api/v1/program/speciality_types', { params })
            .then(({ data: { data: { speciality_types } } }) => {

                context.commit('SET_SPECIALITY_TYPES', speciality_types);

            })
            .catch(() => {

            });
    },
    async GET_GROUPS(context, payload) {


        const params = {
            action: payload.action,
            program_id: payload.program_id,
            show: payload.show,
            with_users: payload.with_users,
        };
        await axios.get('/api/v1/groups', { params })
            .then(({ data: { data: { groups } } }) => {
                context.commit('SET_GROUPS', groups);

            })
            .catch(() => {

            });
    },
    async GET_PROGRAM( context, payload ) { // eslint-disable-line

        await axios.get(`/api/v1/programs/${payload.id}`)
            .then(({ data: { data: { selected, program, teachers } } }) => {
                context.commit('SET_PROGRAM_SELECTED', selected);
                context.commit('SET_PROGRAM', program);
                context.commit('SET_TEACHERS', teachers);

            })
            .catch((error) => {

                return { status: 'error', error };
            });
    },
    async UPDATE_PROGRAM(context, payload) {
        const formData = {
            action: 'data',
            _method: 'patch',
            name: payload.name,
            description: payload.description,
            organization_id: payload.organization_id,
            popularity: payload.popularity,
            main_popularity: payload.main_popularity,
            main_show: payload.main_show ? 1 : 0,
            show: payload.show ? 1 : 0,
            type: payload.type ? payload.type : 'short',
        };
        if (payload.type === 'long') {
            formData.count_semesters = payload.count_semesters;
        }
        try {
            const response = await axios.post(`/api/v1/programs/${payload.program_id}`, formData);
            const { data: { data: { program } } } = response;
            context.commit('SET_PROGRAM', program);
            return { status: 'ok', response: program };
        } catch (error) {
            return { status: 'error', response: error };
        }
    },
    async ADD_PROGRAM(context, payload) {
        const formData = {
            name: payload.name,
            type: payload.type ? payload.type : 'short',
            description: payload.description,
            organization_id: payload.organization_id,
            popularity: payload.popularity,
            main_popularity: payload.main_popularity,
            main_show: payload.main_show ? 1 : 0,
            show: payload.show ? 1 : 0,
        };
        if (payload.type === 'long') {
            formData.count_semesters = payload.count_semesters;
        }

        try {
            const response = await axios.post('/api/v1/programs/', formData);
            const { data: { data: { program } } } = response;
            context.commit('SET_PROGRAM', program);
            return { status: 'ok', response };
        } catch (error) {
            return { status: 'error', response: error };
        }
    },
    SHOW_MODAL(context, payload) { // eslint-disable-line
        if (payload) {
            const modal = context.getters.MODAL;
            if (!modal.includes(payload)) {
                modal.push(payload);
            }
            context.commit('setStore', { key: 'modal.org.addProgram', data: modal }, { root: true });
        }
    },
    HIDE_MODAL(context) {
        // context.commit('SET_INVALID', {});
        context.commit('setStore', { key: 'modal.org.addProgram', data: [] }, { root: true });
    },
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
