// Bashinsky

import * as welcomePage from '@base/api/welcomePage';
import * as pageElement from '@base/api/pageElement';

const data = {
    components: [
        {  },
    ],
    componentsUpdate: [],
    page: [],
    pages: {
        data: [],
        paginate: {},
    },
    pageId: 0,
};


const getters = {
};

const mutations = {
};

const actions = {
    async GET_PAGES({state}, payload ) {
        try {
            const response = await welcomePage.getPages(payload);
            state.pages.data = response.data.data.pages.data;
            const noData = ({ data, ...rest}) => rest;
            state.pages.paginate = noData(response.data.data.pages);
        } catch (e) {
            throw e;
        }
    },

    async GET_PAGE({state}, id) {
        try {
            const response = await welcomePage.getPage(id);
            state.page = response.welcomePage.items;
            state.pageId = response.welcomePage.id;
        } catch (e) {
            throw e;
        }
    },

    async GET_MAIN_PAGE({state}) {
        // try {
            const response = await welcomePage.getMainPage();
            state.page = response.welcomePage.items;
            state.pageId = response.welcomePage.id;
        // } catch (e) {
        //     throw e;
        // }
    },

    async CREATE_PAGE({state}, payload) {
        try {
            const response = await welcomePage.createPage(payload);
            state.pages.data.push(response);
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPDATE_PAGE({state}, payload) {
        try {
            const response = await welcomePage.updatePage(payload);
            state.pages.data = state.pages.data.map((el) => {
                if (el.id === response.welcomePage.id) {
                    return response.welcomePage;
                } else {
                    return el;
                }
            });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async DELETE_PAGE({state}, id) {
        try {
            await welcomePage.deletePage(id);
            state.pages.data = state.pages.data.filter((el => {
                if (el.id !== id) {
                    return el;
                }
            }));
        } catch (e) {
            throw e;
        }
    },

    async ADD_ELEMENT_PAGE({state}, payload) {
        try {
            await pageElement.addElement(payload.page_id, payload.model);
        } catch (e) {
            throw e;
        }
    },

    async UPDATE_ELEMENT_PAGE({state}, payload) {
        try {
            state.components = await pageElement.updateElement(payload.page_id, payload.element_id, payload.model);
        } catch (e) {
            throw e;
        }
    },

    async DELETE_FILE({ state }, payload) {
        try {
            const response = await pageElement.deleteFile(payload);
            state.page = state.page.map(el => el.id === payload.element_id ? response.data.data.welcomePageItem : el)
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async DELETE_ELEMENT_PAGE({state}, payload) {
        try {
            await pageElement.deleteElement(state.pageId, payload.element_id);
            state.page = state.page.filter((el) => {
                if (el.id !== payload.element_id) {
                    return el;
                }
            });
        } catch (e) {
            throw e;
        }
    },
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
