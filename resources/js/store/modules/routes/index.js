// Bashinsky
import * as studyRoute from '@base/api/study-route/study-route'
import { factoryParams } from '@base/api/factoryParams';

const data = {
    studyRoutes: {
        current_page: 1,
        total: 0,
        per_page: 15,
        data: []
    },
    route: null,
    errorPopup: {},
    error: {},
};

const mutations = {
    SET_ROUTE(state, payload) {
        state.route = payload;
    },
    ClEAR_ROUTE(state) {
        state.route = null;
        state.studyRoutes = {
            current_page: 1,
                total: 0,
                per_page: 15,
                data: []
        };
    },
};

const actions = {
    // ============FORM============
    // ============================

    async USER_ROUTES({ state }, payload) {
        try {
            let factory = new factoryParams();

            factory.paginate(payload.studyRoutes)
                  .with(['organization', 'courses', 'form', 'chat'])

            if (payload.filter) {
                factory.filter(payload.filter);
            }

            const response = await studyRoute.userRoutes(factory.getParams());

            state.studyRoutes = response.data.data.studyRoutes;

        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    async REVIEW_ROUTES({ state }, payload) {
        try {
            let factory = new factoryParams();

            const response = await studyRoute.reviewRoutes({
                                    org_id: payload.organization_id,
                                    ...factory.paginate(payload.studyRoutes)
                                              .with(['courses', 'user', 'organization'])
                                              .getParams()
                                });
            state.studyRoutes = response.data.data.studyRoutes;
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },
    async ROUTES_BY_ATTACHED_REGIONS({ state }, payload) {
        try {
            let factory = new factoryParams();
            // console.log(payload.filter)
            const response = await studyRoute.routesByAttachedRegions({
                ...factory.paginate(payload.studyRoutes)
                    .with(['courses', 'user', 'organization', 'chat'])
                    .filter(payload.filter)
                    .getParams()
            });
            state.studyRoutes = response.data.data.studyRoutes;
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    async ROUTE_BY_ATTACHED_REGIONS({ state }, payload) {
        try {
            let factory = new factoryParams();
            const response = await studyRoute.routesByAttachedRegions({
                id: payload.id,
                ...factory.with(['courses', 'user', 'organization', 'chat'])
                    .getParams()
            });
            state.route  = response.data.data.studyRoutes.data[0];
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },


    async ALL_ROUTES({ state }, payload ) {
        try {
            let factory = new factoryParams();
            let { filter, order, search, field, studyRoutes, district_id } = payload;



            const response = await studyRoute.allRoutes(
                factory.paginate(studyRoutes)
                        .with(['courses', 'user', 'organization'])
                        .filter(filter)
                        .search(search)
                        .search_p("district_id", district_id)
                        .getParams()
            )
            state.studyRoutes = response.data.data.studyRoutes;

        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },


    async GER_REVIEW_ROUTE({ state }, payload) {
        try {
            let factory = new factoryParams();

            const response = await studyRoute.reviewRoutes({
                                    org_id: payload.organization_id,
                                    id: payload.id,
                                    ...factory.with(['courses', 'user', 'organization'])
                                              .getParams()
            });
            state.route = response.data.data.studyRoutes.data[0];
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async GER_APPROVING_ROUTE({ state }, payload) {
        try {
            let factory = new factoryParams();

            const response = await studyRoute.approvingRoute({
                org_id: payload.organization_id,
                id: payload.id,
                ...factory.with(['courses', 'user', 'organization'])
                    .getParams()
            });
            state.route = response.data.data.studyRoutes.data[0];
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async STUDY_ROUTES_APPROVING({ state }, payload) {
        try {
            let factory = new factoryParams();
            const response = await studyRoute.studyRoutesApproving({
                ...factory.with(['organization', 'user'])
                    .getParams()
            });
            state.studyRoutes = response.data.data.studyRoutes;
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    async GET_USER_ROUTE({ state }, payload) {
        try {
            let factory = new factoryParams();

            factory.with(['organization', 'courses', 'form', 'chat']);

            const response = await studyRoute.userRoutes({ id: payload.id, ...factory.getParams() });

            state.route = response.data.data.studyRoutes[0];

        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    }
};

export default {
    namespaced: true,
    state: data,
    mutations,
    actions,
};
