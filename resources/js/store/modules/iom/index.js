// Bashinsky

import * as stydyRouteForms from '@base/api/studyRouteForms';

const data = {
    formsIom: [],
    currentForm: {
        id: 0,
        name: '',
        doc_template: '',
    },
};

const mutations = {


    ADD_FORM(state, payload) {
        state.formsIom.push(payload);
    },
    UPDATE_FORM(state, payload) {
        state.formsIom = state.formsIom.map( el => el.id === payload.form_id ? payload.value : el);
    },

    SET_CURRENT_FORM(state, payload) {
        state.currentForm = payload;
    },
    SET_ACTIVE_FORM(state, form_id) {
        state.formsIom = state.formsIom.map(el => { if (el.id === form_id) {
                const form = { ...el, ...{ active: 1 }};
                return form;
            } else {
                const form = { ...el, ...{ active: 0 }};
                return form;
        }} );
    },


    DELETE_FIELD(state, field_id) {
        state.currentForm.fields = state.currentForm.fields.filter( el => {
            if (el.id !== field_id ) {
                return el;
            }
        });
    },
};

const actions = {
    // ============FORM============
    // ============================

    async GET_FORMS({ state }, payload) {
        try {
            const response = await stydyRouteForms.getForms();
            state.formsIom = response.data.data.studyRouteForms.data;
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async SUBMIT_FORM({ dispatch }, payload) {
        try {
            if (payload.id) {
                dispatch('UPDATE_FORM', payload);
            } else {
                dispatch('CREATE_FORM', payload);
            }
        } catch (e) {
            throw e;
        }
    },

    async CREATE_FORM({ commit }, payload) {
        try {
            const response = await stydyRouteForms.createForms(payload.name);
            commit('ADD_FORM', response.data.data.studyRouteForm );
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPDATE_FORM({ commit }, payload) {
        try {
            const response = await stydyRouteForms.updateForms(payload);
            commit('UPDATE_FORM', { form_id: payload.id,  value: response.data.data.studyRouteForm});
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async SET_ACTIVE_FORM({ commit }, form_id) {
        try {
            const response = await stydyRouteForms.setActive(form_id);
            commit('SET_ACTIVE_FORM', form_id);
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    // ============END-FORM============
    // ================================


    // ============FIELD===============
    // ================================

    async SUBMIT_FIELD({ dispatch }, payload) {
        try {
            if (payload.value.id) {
                dispatch('UPDATE_FIELD', payload);
            } else {

                dispatch('CREATE_FIELD', payload);
            }
        } catch (e) {
            throw e;
        }
    },

    async CREATE_FIELD({ commit }, payload) {
        try {
            const response = await stydyRouteForms.createField(payload);
            commit('SET_CURRENT_FORM', response.data.data.studyRouteForm );
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPDATE_FIELD({ commit }, payload) {
        try {
            const response = await stydyRouteForms.updateField(payload);
            commit('SET_CURRENT_FORM', response.data.data.studyRouteForm );
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async DELETE_FIELD({ commit }, payload) {
        try {
            await stydyRouteForms.deleteField(payload);
            commit('DELETE_FIELD', payload.field_id);
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    // ============END-FIELD===========
    // ================================


};

export default {
    namespaced: true,
    state: data,
    mutations,
    actions,
};
