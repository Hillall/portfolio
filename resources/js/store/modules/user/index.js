import axios from '@base/bootstrap';

const data = {
    organizations: null,
};


const getters = {
    ORGANIZATIONS: (state) => state.organizations,
};

const mutations = {
    SET_ORGANIZATIONS: (state, payload) => {
        state.organizations = payload; // eslint-disable-line
    },
};

const actions = {
    async GET_ORGANIZATIONS(context, payload) {


        const params = {

        };
        await axios.get(`/api/v1/users/${payload.user_id}`, { params: params })
            .then(({ data: { data: { user: { organizations }} } }) => {
                context.commit('SET_ORGANIZATIONS', organizations);

            })
            .catch(() => {

            });
    },
    async setReadNotification(context, payload) {
        const params = {
            notification_id: payload.notification_id
        };
        await axios.post(`/api/v1/user/`+payload.user_id+`/setReadNotification`, params)
            .then();
    }
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};

