import axios from '@base/bootstrap';

const state = {
    tasks: [],
    task: {},
};

const getters = {
    MODAL_ADD: (state, getters, rooState, rootGetters) => rootGetters.getStore('modal.org.lesson.task.add'),
    MODAL_EDIT: (state, getters, rooState, rootGetters) => rootGetters.getStore('modal.org.lesson.task.edit'),
    TASKS: (state) => state.tasks,
    TASK: (state) => state.task,
};

const mutations = {
    SET_TASK: (state, payload) => {
        state.task = payload
    },
    ADD_TASK: (state, payload) => state.tasks.push(payload),
};

const actions = {
    manage_answer: async (context, payload) => {


        const formData =  new FormData();
        formData.append('_method', 'patch');
        formData.append('action', payload.action);
        formData.append('answer_id', payload.answer_id);


        try {
            const response = await axios.post(`/api/v1/tasks/${payload.task_id}`,  formData)
                .then(({ data: { data: { task } } }) => {
                    context.commit('SET_TASK', task);
                });


            return { status: 'ok', data: response.data };
        }catch (error) {

            return { status: 'error', error };
        }
    },
    remove_answer_documnet: async (context, payload) => {


        const formData =  new FormData();
        formData.append('_method', 'patch');
        formData.append('action', 'remove-answer-doc');
        formData.append('answer_id', payload.answer_id);
        formData.append('document_id', payload.document_id);

        try {
            const response = await axios.post(`/api/v1/tasks/${payload.task_id}`,  formData)
                .then(({ data: { data: { task } } }) => {
                    context.commit('SET_TASK', task);
                });


            return { status: 'ok', data: response.data };
        }catch (error) {

            return { status: 'error', error };
        }
    },
    save_check_answer: async (context, payload) => {

        const formData =  new FormData();

        formData.append('_method', 'patch');
        formData.append('action', 'save-to-check-answer');
        formData.append('text_answer', payload.text_answer);


        if (typeof payload.documents === 'object') {
            Object.keys(payload.documents).forEach((k) => {
                formData.append(`documents[${k}]`, payload.documents[k]);
            });
        }

        try {
            const response = await axios.post(`/api/v1/tasks/${payload.task_id}`,  formData)
                .then(({ data: { data: { task } } }) => {
                    context.commit('SET_TASK', task);
                });


            return { status: 'ok', data: response.data };
        }catch (error) {

            return { status: 'error', error };
        }
    },
    SAVE_ANSWER: async (context, payload) => {

        const formData =  new FormData();

        formData.append('_method', 'patch');
        formData.append('action', 'set-answer');
        formData.append('text_answer', payload.text_answer);


        if (typeof payload.documents === 'object') {
            Object.keys(payload.documents).forEach((k) => {
                formData.append(`documents[${k}]`, payload.documents[k]);
            });
        }

        try {
            const response = await axios.post(`/api/v1/tasks/${payload.task_id}`,  formData)
                .then(({ data: { data: { task } } }) => {
                    context.commit('SET_TASK', task);
                }).catch((error) => {
                    if (error.response.status == 413){
                        alert('Файл слишком большой');
                    }
                    console.warn(error);
                });


            return { status: 'ok', data: response.data };
        }catch (error) {


            return { status: 'error', error };
        }
    },
    GET_TASK: async (context, payload) => {



        const url = `/api/v1/tasks/${payload.task_id}`;
        const formData = new FormData();
        try {
            const response = await axios.get(url)
                .then(({ data: { data: { task } } }) => {
                    context.commit('SET_TASK', task);
                });


            return { status: 'ok', data: response.data };
        } catch (error) {

            return { status: 'error', error };
        }
    },
};

export default {
    namespaced: true,
    state: state,
    mutations,
    getters: getters,
    actions,
};
