import axios from '@base/bootstrap';
import { defLesson } from '@base/inc/defaults/app';
import { detailedLesson } from '@base/api';
import { getElementLesson, deleteElementLesson } from '@base/api/lesson';
import { getGradeBook } from '@base/api/gradebook';
import { createLesson, updateLesson } from '@base/api/lesson';

const data = {
    lessons: [{ ...defLesson },],
    teachers: [],
    elements: [],
    scores: {},
    lessonsNormal: []
};

const getters = {
    MODAL: (state, getters, rooState, rootGetters) => rootGetters.getStore('modal.org.addLesson'), // eslint-disable-line
    LESSONS: (state) => state.lessons,
    SCORES: (state) => state.scores,
    LESSON: (state) => (id) => {
        if (state.lessons !== null) { return state.lessons.find((el) => el.id === id); }
        return { ...defLesson };
    },
    TEACHERS: (state) => state.teachers,
    LESSON_TEACHERS: (state) => (id) => state.teachers.filter((el) => el.lesson_id === id),
    ELEMENTS_LESSON: (state) => (id) => state.elements.filter((el) => el.lesson_id === id),
};

const mutations = {
    SET_LESSON: (state, payload) => state.lessons = payload, // eslint-disable-line
    SET_SCORES: (state, payload) => state.scores = payload,
    UPDATE_LESSON: (state, payload) => state.lessons = state.lessons.map((el) => {  // eslint-disable-line
        if (el.id === payload.id) { return payload; }
        return el;
    }),
    DELETE_LESSON: (state, payload) => {
        state.lessons = state.lessons.filter(el => el.id !== payload); // eslint-disable-line
        state.teachers = state.teachers.filter(el => el.lesson_id !== payload); // eslint-disable-line
    }, // eslint-disable-line
    DELETE_TEACHER: (state, payload) => {
        const buffer = [];
        for (let teacher of state.teachers) { // eslint-disable-line
            if ((teacher.lesson_id === payload.lesson_id) && (teacher.id === payload.teacher_id)) {
            } else {
                buffer.push(teacher);
            }
        }
        state.teachers = buffer;// eslint-disable-line
    },
    SET_TEACHERS: (state, payload) => state.teachers = payload, // eslint-disable-line
    ADD_TEACHERS: (state, payload) => {
        if (state.teachers.find(payload.lesson_id)) {
            state.teachers = state.teachers.map(el => { // eslint-disable-line
                if (el.lesson_id === payload.lesson_id) {
                    return payload;
                }
                return el;
            });
        } else {
            state.teachers.push(payload);
        }
    },
    UPDATE_TEACHERS: (state, payload) => {
        const buffer = state.lessons.filter((el) => el.lesson_id !== payload.teachers[0].lesson_id);
        state.teachers = buffer.concat(payload.teachers); // eslint-disable-line
    },
    ADD_ELEMENTS: (state, payload) => state.elements.push(payload),
    UPDATE_ELEMENTS: (state, payload) => {
        const lesson = state.lessons.find((les) => les.id === payload.lesson_id);
        if (typeof lesson === 'object') {
            lesson.elements.push(payload);
        }
    },
    DELETE_ELEMENT: (state, payload) => {
        const lesson = state.lessons.find((les) => les.id === payload.lesson_id);
        if (typeof lesson === 'object') {
            lesson.elements = lesson.elements.filter((el) => el.id !== payload.element_id);
        }
    }, // eslint-disable-line
};

const actions = {
    GET_LESSONS_WITH_DISCIPLINE: async (context, payload) => {
        const les = [];
        const all = [];

        for (const id of payload) {

            all.push(axios.get('/api/v1/lessons/', { params: { course_id: id } })
                .then(({ data: { data: { lessons,  } } }) => {
                    les.push(...lessons);
                }));
        }
        await Promise.all(all);
        const idLes = les.map((el) => el.id);

        context.commit('SET_LESSON', les);
        context.dispatch('GET_TEACHERS_WITH_DISCIPLINE', idLes);
        //context.dispatch('GET_LESSONS_ELEMENT', idLes);
    },
    GET_SCORES: async ({  dispatch, commit, rooState, rootGetters }, payload) => {
        let score = {};
        const all =[];
            for (const id of payload) {
                all.push(getGradeBook({  user_id: rootGetters.auth.id, course_id: id, 'group_by': ['course_id', 'course_module_id']})
                            .then(({ data: { data: { gradebooks } } }) => {
                                score = Object.assign(score, gradebooks);
                            })
                );
            }
        //};
        await Promise.all(all);
        commit('SET_SCORES', score);
    },
    GET_TEACHERS_WITH_DISCIPLINE: async ({state}, payload) => {
        const all = [];
        for (const id of payload) {

            all.push(detailedLesson(id)
                .then((response) => {
                    return { ...response.data.data.lesson, course: response.data.data.course, teachers: response.data.data.teachers };
                }));
        }
        const response = await Promise.all(all);
        state.lessons = response;
    },
    GET_LESSONS_ELEMENT: async ({ state }, payload) => {
        const lessons = {};
        const all = [];
        for (const lessonId of payload) { // eslint-disable-line
            all.push(getElementLesson(lessonId, 15, 1)
                .then((response) => {
                    lessons[lessonId] = { ...response };
                    return { [lessonId]: { ...response }}
                }
            ));
        }
        const response = await Promise.all(all);
        state.elements = Object.values(response);
    },
    ADD_LESSON: async ({ state }, payload) => {
        try {
            const response = await createLesson(payload);
            state.lessons.push(response.data.data.lesson);
            return response.data.data.lesson;
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    UPDATE_LESSON: async (context, payload) => {
        try {
            const response = await updateLesson(payload);
            context.commit('UPDATE_LESSON', response.data.data.lesson);
            return response.data.data.lesson;
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },
    DELETE_LESSON: (context, payload) => {
        const formData = new FormData();
        formData.append('_method', 'delete');
        axios.delete(`/api/v1/lessons/${payload}`, formData)
            .then(() => {

                context.commit('DELETE_LESSON', payload);
            })
            .catch(() => {

            });
    },
    DELETE_ELEMENT: (context, payload) => {
        deleteElementLesson(payload.element_id)
            .then(() => context.commit('DELETE_ELEMENT', payload));
    },
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
