// Bashinsky
import * as basket from '@base/api/study-route/basket';

const data = {
    basket: {
        courses: [],
        events: [],
    },

};

const actions = {
    async ADD_TO_BASKET({ state }, payload) {
        try {
            const response = await basket.addToRequest({ id: payload.course.id, type: payload.type });
            state.basket[payload.type].push(payload.course);
        } catch (e) {
            throw e;
        }
    },
    async DELETE_FROM_BASKET({ state }, payload) {
        try {
            const response = await basket.delFromRequest(payload);
            state.basket[payload.type] = state.basket[payload.type].filter(el => el.id !== payload.id)
        } catch (e) {
            throw e;
        }
    },
    async GET_BASKET({ state }) {
        try {
            const response = await basket.getBasket();
            state.basket = response.data.data.data;
        } catch (e) {
            throw e;
        }
    },
    async CHECK_OUT({state}, payload) {
        try {
            const response = await basket.checkout(payload.organization_id, payload.quiz_attempt_id);
            state.basket = { courses: [], events: []}
            return response;
        } catch (e) {
            throw e.response;
        }
    }
};

export default {
    namespaced: true,
    state: data,
    actions,
};
