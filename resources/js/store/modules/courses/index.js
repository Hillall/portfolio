// Bashinsky

import Paginate from '@base/inc/func/Paginate';
import * as courses  from '@base/api/courses';

import { defLesson } from '@base/inc/defaults/app';
import { detailedLesson, attachOrDetachUser } from '@base/api';
import { getElementLesson, deleteElementLesson } from '@base/api/lesson';
import { getGradeBook } from '@base/api/gradebook';
import { createLesson, updateLesson, deleteLesson } from '@base/api/lesson';

import {
    uploadElementFile,
    uploadElementImg,
    uploadElementPage,
    uploadElementLink,
    createTask,
    elementTypes,
    uploadElementTest,
} from '@base/api/lesson';

const data = {
    courses: [],
    program: null,
    course_selected: false,
    paginate: {
        page: {
            0: 0,
        },
    },
    gradebook: {},
    quantityTotal: 10,
    quantityCourses: 0,

    currentCourseDetail: {
        curr_course: null,
        semesters: [],
        sub_courses: null,
        course_modules: null,
        elements: null,
    },

    courseTeachers: null,
    lessons: [{ ...defLesson },],
    teachers: [],
    elements: [],
    scores: {},
    lessonsNormal: [],
};

const getters = {
    COURSES: (state) => state.courses,
    PROGRAM: (state) => state.program,
    COURSE: (state) => (id) => {
        if (state.courses !== null) { return state.courses.find((el) => el.id === id); }
        return null;
    },
    PAGINATE: (state) => state.paginate,
    QUANTITY: (state) => state.quantityTotal,
    QUANTITY_COURSES: (state) => state.quantityCourses,
    MODAL: (state, getters, rooState, rootGetters) => rootGetters.getStore('modal.org.addCourse'), // eslint-disable-line
    COURSE_GRADEBOOK: (state) => state.gradebook,
    COURSE_SELECTED: (state) => state.course_selected,
    COURSE_TEACHERS: (state) => state.courseTeachers,
    COURSE_DETAIL: (state) => state.currentCourseDetail,
    QUANTITY_LESSONS: (state) => {
        let length = 0
        if (state.currentCourseDetail.course_modules) {
            state.currentCourseDetail.course_modules.forEach((el) => length = length + el.size );
        }
        return length;
    }
};

const mutations = {
    SET_COURSE: (state, payload) => {
        state.courses = payload; // eslint-disable-line
    },
    SET_COURSE_GRADEBOOK: (state, payload) => {
        state.gradebook = payload; // eslint-disable-line
    },
    UPDATE_COURSE_TEACHERS: (state, payload) => {
        state.courseTeachers = [...state.courseTeachers, ...payload]; // eslint-disable-line
    },
    ADD_COURSE(state, payload) {
        state.courses.push(payload); // eslint-disable-line
    },
    SET_PAGINATE: (state, payload) => {
        state.paginate = Paginate.calculate(payload); // eslint-disable-line
    },
    SET_QUANTITY_COURSES: (state, payload) => {
        state.quantityCourses = payload; // eslint-disable-line
    },
    UPDATE_COURSES: (state, payload) => {
        state.currentCourseDetail.curr_course = payload;
    },
    //lessons
    DELETE_LESSON: (state, payload) => {
        state.currentCourseDetail.course_modules.get(payload.module_id)
                                                .delete(payload.lesson_id);

        state.currentCourseDetail.course_modules = new Map(state.currentCourseDetail.course_modules);
    },
    UPDATE_LESSON: (state, payload) => {
        state.currentCourseDetail.course_modules.get(payload.key)
                                                .set(payload.lesson.id, payload.lesson);
        state.currentCourseDetail.course_modules = new Map(state.currentCourseDetail.course_modules);
    },
    ADD_ELEMENT: (state, payload) => {
        state.currentCourseDetail.elements.get(payload.key)
                                          .set(payload.element.id, payload.element);
    },
    DELETE_LESSON_ELEMENT: (state, payload) => {
        state.currentCourseDetail.elements.get(payload.lesson_id)
                                        .delete(payload.element_id);
        state.currentCourseDetail.elements = new Map(state.currentCourseDetail.elements);
    },

    PUSH_DISCIPLINE: (state, payload) => {
        state.currentCourseDetail.sub_courses.set(payload.id, payload);
        state.currentCourseDetail.course_modules.set(payload.id, new Map());
        state.currentCourseDetail.sub_courses = new Map(state.currentCourseDetail.sub_courses);
    },

    UPDATE_DISCIPLINE: (state, payload) => {
        state.currentCourseDetail.sub_courses.set(payload.id, payload);
        state.currentCourseDetail.sub_courses = new Map(state.currentCourseDetail.sub_courses);
    },

    DELETE_DISCIPLINE: (state, payload) => {
        console.log('payload', payload);
        state.currentCourseDetail.course_modules.delete(payload);
        state.currentCourseDetail.sub_courses.delete(payload);
        state.currentCourseDetail.course_modules = new Map(state.currentCourseDetail.course_modules);
    },
    SET_CURRENT_COURSE_DETAIL: (state, payload) => {
        // const noSubCourses = ({ sub_courses, ...rest }) => rest;
        const noCourseModules = ({ course_modules, ...rest}) => rest;
        const noElements = ({ elements, ...rest}) => rest;

        const noId = ({ id, ...rest }) => rest;

        let subCourses = new Map();
        let courseModules = new Map();
        let elements = new Map();

        payload.sub_courses.forEach(sc => {
            subCourses.set(sc.id, noCourseModules(sc));
            courseModules.set(sc.id, new Map(sc.course_modules.map(cm => {
                if (cm.elements){
                    elements.set(cm.id, new Map(cm.elements.map(el => [el.id, el])));
                }
                return [cm.id, noElements(cm)];
            })));
        });

        state.currentCourseDetail.curr_course = payload;
        state.currentCourseDetail.sub_courses = subCourses;
        state.currentCourseDetail.course_modules = courseModules;
        state.currentCourseDetail.elements = elements;
    },
    UPDATE_USERS: (state, payload) => {
        state.currentCourseDetail.curr_course.users = payload;
        state.currentCourseDetail = { ...state.currentCourseDetail };
    },
}

const actions = {
    GET_COURSE: async ( context, payload ) => { // eslint-disable-line
        try {
            const { data: { data: { courses, gradebook } } } = await courses.getCourses(payload);
            // todo проверить там где используется
            context.commit('SET_COURSE', courses.data);
            context.commit('SET_PAGINATE', courses);
            context.commit('SET_QUANTITY_COURSES', courses.total);

            if (gradebook) {
                context.commit('SET_COURSE_GRADEBOOK', gradebook);
            }
        } catch (e) {
            throw e.response.data.errors
        }
    },
    async GET_COURSE_DETAIL({ commit, state }, payload) {
        // try {
            const response = await courses.getCourse(payload.id);

            commit('SET_CURRENT_COURSE_DETAIL', response.data.data.course);
            state.courseTeachers = response.data.data.teachers;
            state.course_selected = response.data.data.selected;
            if (response.data.data.gradebook) state.gradebook = response.data.data.gradebook;
            return response.data.data.course;
        // } catch (e) {
        //     console.warn(e);
        // }
    },
    async ADD_COURSE(context, payload) {
        try {
            const response = await courses.addCourse(payload);
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    async FREE_SINGUP(context, payload) {
        const { data: { data: { course } } } = await courses.freeSignup(payload);
        context.commit('UPDATE_COURSES', course);
        return course;
    },
    UPDATE_COURSE: async (context, payload) => {
        try {
            const { data: { data: { course } } } = await courses.updateCourse(payload);
            context.commit('UPDATE_COURSES', course);
            return course;
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    DELETE_LESSON: async (context, payload) => {
        try {
            await deleteLesson(payload.lesson_id);
            context.commit('DELETE_LESSON', payload);
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    ADD_LESSON: async ( context , payload) => {
        try {
            const { data: { data: { lesson }}} = await createLesson(payload);
            context.commit('UPDATE_LESSON', { lesson: lesson, key: payload.course_id });
            return lesson;
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    UPDATE_LESSON: async (context, payload) => {
        try {
            const {data: { data: { lesson }}} = await updateLesson(payload);
            context.commit('UPDATE_LESSON', { lesson: lesson, key: lesson.course_id });
            return lesson;
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    MANAGE_LESSON_USER: async (context, payload) => {
        try {
            const { data: { data: { lesson } } } = await attachOrDetachUser({ id: payload.lesson_id,
                                       to: 'lessons',
                                       user_id: payload.user_id },
                                       payload.action);
            context.commit('UPDATE_LESSON', { lesson: lesson, key: lesson.course_id });
            return lesson
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    DELETE_LESSON_ELEMENT: async (context, payload) => {
        try {
            await deleteElementLesson(payload.element_id);
            context.commit('DELETE_LESSON_ELEMENT', payload)
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },
    async ADD_DISCIPLINE(context, payload) {
        try {
            const response = await courses.createDiscipline(payload);
            context.commit('PUSH_DISCIPLINE', response.data.data.course);
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },
    async UPDATE_DISCIPLINE(context, payload) {
        try {
            const response = await courses.updateDiscipline(payload);
            context.commit('UPDATE_DISCIPLINE', response.data.data.course);
        } catch (e) {
            throw e.response.data.errors;
        }
    },
    async DELETE_DISCIPLINE(context, payload) {
        try {
            await courses.deleteDiscipline(payload.id);
            context.commit('DELETE_DISCIPLINE', payload.id);
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }
    },

    async UPLOAD_ELEMENT_LINK(context, payload) {
        try {
            const response = await uploadElementLink(payload);
            context.commit('ADD_ELEMENT', { element: response.element, key: payload.lesson_id });
        } catch (e) {
            console.warn(e);
            throw e.response.data.errors;
        }

    },

    async UPLOAD_ELEMENT_IMG(context, payload) {
        try {
            const response = await uploadElementImg(payload);
            context.commit('ADD_ELEMENT', { element: response.element, key: payload.lesson_id });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPLOAD_ELEMENT_PAGE(context, payload) {
        try {
            const response = await uploadElementPage(payload);
            context.commit('ADD_ELEMENT', { element: response.element, key: payload.lesson_id });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPLOAD_ELEMENT_TEST(context, payload) {
        try {
            const response = await uploadElementTest(payload);
            context.commit('ADD_ELEMENT', { element: response.element, key: payload.lesson_id });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async UPLOAD_ELEMENT_FILE(context, payload) {
        try {
            const response = await uploadElementFile(payload);
            context.commit('ADD_ELEMENT', { element: response.element, key: payload.lesson_id });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async CREATE_ELEMENT_TASK(context, payload) {
        try {
            const { data: { data: { element } } } = await createTask(payload);
            context.commit('ADD_ELEMENT', { element: element, key: payload.lesson_id });
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async ATTACH_USER(context, payload) {
        try {
            const { data: { data: { teachers } } } = await attachOrDetachUser({ ...payload, ...{ to: 'courses' } }, 'attach-user');
            context.commit('UPDATE_USERS', teachers);
        } catch (e) {
            throw e.response.data.data.errors;
        }
    },

    async DETACH_USER(context, payload) {
        try {
            const { data: { data: { teachers } } } = await attachOrDetachUser({ ...payload, ...{ to: 'courses' } }, 'detach-user');
            context.commit('UPDATE_USERS', teachers);
        } catch (e) {
            throw e.response.data.data.errors;
        }
    }
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
