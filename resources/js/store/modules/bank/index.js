import { getListBank } from '@base/api/bank-question/bank';
import { getListQuestions,  delQuestion} from '@base/api/bank-question/question';

const data = {
    bankQuestion: [],
    bankCategory: {},
    listQuestion: {
        questions: {
            data: 0
        }
    },
    base_category_id: null,
    category_id: null,
    interview: {},
};

const getters = {
};

const mutations = {
    DELETE_QUESTION(state, payload) {
        state.listQuestion.questions.data = state.listQuestion.questions.data.filter(el => el.id !== payload.id);
    }
};

const actions = {
    async GET_BANK({ state }, payload) {
        if (payload.course_id) {
            state.bankQuestion = await getListBank({ course_id: payload.course_id, 'with[]': 'tree-childs' });
        }
        
        if (payload.organization_id) {
            state.bankQuestion = await getListBank({ organization_id: payload.organization_id, 'with[]': 'tree-childs' });
        }

        if (state.bankQuestion[0].id) {
            const bank_id = state.bankQuestion[0].id;
            state.category_id = bank_id;
            state.base_category_id = bank_id;
            state.listQuestion = await getListQuestions({ bank_id: bank_id });
            state.bankCategory = await getListBank({ id: bank_id, 'with[]': 'tree-childs' });
        }
        if (payload.organization_id) {
            state.bankQuestion = await getListBank({ organization_id: payload.organization_id, 'with[]': 'tree-childs' });
            state.category_id = state.bankQuestion[0].id;
            state.base_category_id = state.bankQuestion[0].id;
            state.listQuestion = await getListQuestions({ bank_id: state.bankQuestion[0].id });
            state.bankCategory = await getListBank({ id: state.bankQuestion[0].id, 'with[]': 'tree-childs' });
        }
    },
    async GET_CATEGORY({ state }, payload) {
        if (state.base_category_id && payload.category_id) {
            state.bankCategory = await getListBank({ id: state.base_category_id, 'with[]': 'tree-childs' });
            state.bankQuestion = await getListBank({ id: payload.category_id, 'with[]': 'tree-childs' });
            state.listQuestion = await getListQuestions({ bank_id: payload.category_id });
        }
    },
    async GET_QUESTION({state}, payload) {
        state.listQuestion = await getListQuestions({ bank_id: payload.category_id });
    },
    async DELETE_QUESTION(context, payload) {
        try {
            await delQuestion(payload.id)
                        .then(() => {
                            context.commit('DELETE_QUESTION', payload);
                        });
        } catch (e) {
            return e;
        }
    }, 
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
