// Bashinsky
import {
    getListTest,
    createAttempt,
    createCustomScales,
    updateTestSettings,
    removeQuestionRule } from '@base/api/test/adding-questions';

import { getListBank } from '@base/api/bank-question/bank';
import {
            quizAttempts,
            answer,
            resultOfUserAttempt,
            finishAttempt
        }
from '@base/api/test/test-attempt';

function settingsAdapter(quiz, modelName, mode) {
    if (mode === 'response') {
        return {
            start_time_availability: quiz.start_time_availability,
            passing_score: quiz.passing_score,
            end_time_availability: quiz.end_time_availability,
            lead_time: quiz.lead_time,
            name: quiz.name,
            protected: quiz.protected ? true : false,
            change_answers: quiz.params.change_answers ? true : false,
            count_attempts: quiz.params.count_attempts,
            see_result_questions: quiz.params.see_result_questions ? true : false,
        }
    } else if (mode === 'request') {
        const request = {
            passing_score: quiz.passing_score,
            end_time_availability: quiz.end_time_availability,
            lead_time: quiz.lead_time,
            name: quiz.name,
            change_answers: quiz.change_answers ? 1 : 0,
            see_result_questions: quiz.see_result_questions ? 1 : 0,
            protected: quiz.protected ? 1 : 0,
            count_attempts: quiz.count_attempts,
        }
        request.protected ? request.password = quiz.password : {};

        return request;
    }
}



const data = {
    test: {},
    treeBank: {},
    attempt: {
    },
    questionsAttempt: {},
    currentQuestion: {},
    currentCorrectQuestion: [],
    questionAnswer: {},
    questionAnswerAudio: [],
    questionAnswerFiles: [],
    resultsAttemptUser: {},
    currentAttempt: {},
    currentGrade: {},
    scales: [],
    settings: {},
    front_result: {},
    server_time: null,
};


const getters = {
    TEST: (state) => state.test,
    TREE_BANK: (state) => state.treeBank,
    ATTEMPT: (state) => state.attempt,
    QUESTIONS_ATTEMPT: (state) => state.questionsAttempt,
    CURRENT_QUESTION_ATTEMPT: (state) => state.currentQuestion,
    QUESTION_ANSWER: (state) => state.questionAnswer,
    QUESTION_ANSWER_AUDIO: (state) => state.questionAnswerAudio,
    QUESTION_ANSWER_FILES: (state) => state.questionAnswerFiles,
    RESULTS_ATTEMPT_USER: (state) => state.resultsAttemptUser,
    CURRENT_ATTEMPT: (state) => state.currentAttempt,
    CURRENT_GRADE: (state) => state.currentGrade,
    AVAILABILITY_STATUS: (state) => {
        if (!isNaN(Date.parse(state.currentAttempt.finished_at))) {
            if ((new Date().getTime() - Date.parse(state.currentAttempt.finished_at)) > 0) {
                return false;
            } else {
                return true;
            }
        }
        else return false;
    },
};

const mutations = {
    SET_TEST: (state, payload) => {
        state.test = payload; // eslint-disable-line
    },
    SET_ATTEMPT: (state, payload) => {
        state.attempt = payload;
    },
    SET_TREE_BANK: (state, payload) => {
        state.treeBank = payload;
    },
    SET_QUESTIONS_ATTEMPT: (state, payload) => {
        state.attempt = payload;
        state.front_result = payload.front_result;
        state.questionsAttempt = payload.attempt_questions;
        const answer = Object.keys(payload.attempt_questions).map((key) => ({[Number(key) + 1] : payload.attempt_questions[key].attempt_answer ? payload.attempt_questions[key].attempt_answer.value : null }));
        const answerAudio = Object.keys(payload.attempt_questions).map((key) => ({[Number(key) + 1] : payload.attempt_questions[key].attempt_answer ? payload.attempt_questions[key].attempt_answer.audio : [] }));
        const answerFiles = Object.keys(payload.attempt_questions).map((key) => ({[Number(key) + 1] : payload.attempt_questions[key].attempt_answer ? payload.attempt_questions[key].attempt_answer.files : [] }));

        state.questionAnswer = answer.reduce((a, b) => Object.assign({}, a, b));
        state.questionAnswerAudio = answerAudio.reduce((a, b) => Object.assign({}, a, b));
        state.questionAnswerFiles = answerFiles.reduce((a, b) => Object.assign({}, a, b));


        const noQuestions = ({ attempt_questions_results, attempt_questions,  ...rest }) => rest
        state.currentAttempt = noQuestions(payload);

    },
    SET_CURRENT_QUESTION: async (state, id) => {
        state.currentQuestion = await state.questionsAttempt[id];
        state.currentGrade = { point_count: state.questionsAttempt[id].point_count, score: state.questionsAttempt[id].score };

        if (state.front_result){
            let array = [];

            if (state.front_result.result[id].correct_answer){

	            for (var k in state.front_result.result[id].correct_answer) {
		            array.push(state.front_result.result[id].correct_answer[k].id)
		        }
            }

            state.currentCorrectQuestion = array;
            //state.currentCorrectQuestion = state.front_result.result[id].correct_answer ? state.front_result.result[id].correct_answer.map((el) => el.id) : [];
        }
    },
    SET_CHANGED_POSITION_QUESTION: (state, payload) => {
        state.questionsAttempt[payload.id - 1].common_question.question_answers = payload.value;
    },
    SET_QUESTION_ANSWER: (state, payload) => {
        state.questionAnswer = { ...state.questionAnswer, ...payload };
    },
    DELETE_ANSWER_EL: (state, payload) => {
        if (state.questionAnswer[payload.id])
        state.questionAnswer[payload.id] = state.questionAnswer[payload.id]
                                                .filter( el => el !== payload.value );
    },
    ADD_ANSWER_EL: (state, payload) => {
        console.log('payload', payload);
        if (state.questionAnswer[payload.id])
            state.questionAnswer[payload.id] = [...new Set(state.questionAnswer[payload.id].concat([payload.value]))]
        else state.questionAnswer[payload.id] = [ payload.value ];
    },
    ADD_ONE_CHOOSE_ANSWER_EL: (state, payload) => {
        state.questionAnswer[payload.id] = payload.value;
        //state.questionAnswer[payload.id] = payload.value.text;
    },
    ADD_TEXT_AUDIO_FILES_EL: (state, payload) => {
        console.log(payload);
        if (payload.value.text) state.questionAnswer[payload.id] = payload.value.text;
        if (payload.value.audio) state.questionAnswerAudio[payload.id] = payload.value.audio;
        if (payload.value.files) state.questionAnswerFiles[payload.id] = payload.value.files;
    },
    DELETE_AUDIO_FILES_EL: (state, payload) => {

    },
    CHANGE_RELATION_ANSWER_EL: (state, payload) => {
        state.questionAnswer[payload.id] = payload.value;
    },
};



const actions = {
    async GET_TEST({ state }, payload) {
        try {
            const test = await getListTest({ ...payload, ...{ 'with': ['questions', 'model'] } })
            state.test = test;
            state.settings = settingsAdapter(test[0], test[0].model.name, 'response');
            return state.settings;
        }
        catch(error) {
            return error;
        }
    },
    async CREATE_ATTEMPT({ state }, payload) {
        try {
            const response = await createAttempt(payload);
            state.attempt = response.data.data.attempt;
        } catch (e) {
            throw e;
        }
    },
    async GET_BANK(context, payload) {
        context.commit('SET_TREE_BANK', await getListBank(payload));
    },
    async START_TEST({ state }, payload) {
        try {
            const response = await createAttempt({ test_id: state.test[0].id, password: payload.password });
            state.attempt = response.data.data.attempt;
            return response.data.data.attempt
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async START_TEST_BY_ID({ state }, payload) {
        try {
            const response = await createAttempt({ test_id: state.test_id, password: payload.password });
            state.attempt = response.data.data.attempt;
        } catch (e) {
            throw e.response.data.errors;
        }
    },

    async GET_QUESTIONS_ATTEMPT(context, id) {
        context.commit('SET_QUESTIONS_ATTEMPT', await quizAttempts({ id: id, 'with': ['questions', 'questions_results', 'attempt_result', 'front_result'] }));
        context.commit('SET_CURRENT_QUESTION', 0);
    },
    async CHANGE_CURRENT_QUESTION(context, id) {
        context.commit('SET_CURRENT_QUESTION', id);
    },
    async SEND_ANSWER(context, id, payload ) {
        await answer(id, payload);
    },
    async GET_RESULTS_ATTEMPT_USER({ state }, payload) {

        const res = await resultOfUserAttempt(payload);
        const noData = ({ data, ...rest }) => rest;
        state.resultsAttemptUser = { data: res.data, paginate: noData(res) };
    },
    async GET_SCALES({ state }, payload) {
        state.scales = await getListTest({ id: payload.id, with: ['grading_scales'] })
                                .then(response => {
                                    return response[0].grading_scales;
                                });
    },
    async UPDATE_SCALES({ state }, payload) {
        let prevValue = {};
        let nextValue = {};

        let scales = state.scales.map( (el, index) => {
            if (payload.index === index) {

                index ? prevValue = { id: index - 1, value: payload.value.start_score - 1 } : {};
                index !== state.scales.length ? nextValue = {id: index + 1, value: payload.value.end_score + 1 } : {};
                return payload.value;
            } else {
                return { grade: el.grade, start_score: el.start_score, end_score: el.end_score};
            }
        }, prevValue, nextValue );

        // if (payload.index > length) {
        //     scales.push(payload.value);
        // }

        Object.keys(prevValue).length !== 0 ? scales[prevValue.id].end_score = prevValue.value : {};
        scales[nextValue.id] ? scales[nextValue.id].start_score = nextValue.value : {};

        return await new Promise((resolve, reject) => {
            createCustomScales({ scales: scales, id: payload.test_id })
            .then((response) => {
                const scales = response.scales.map(el => {
                    return { grade: el.grade, start_score: el.start_score, end_score: el.end_score}
                })
                state.scales = scales;
                resolve(scales);
            } )
            .catch(error => {
                reject(error);
            });
        });
    },

    async UPDATE_TEST_SETTINGS({ state }, payload) {
        try {
            const reqSett = settingsAdapter(payload, payload.name, 'request');
            const response = await updateTestSettings(state.test[0].id, reqSett);
            const settings = settingsAdapter(response, response.name, 'response');
            state.settings = settings;
            return state.settings;
        } catch (e) {
            throw e;
        }
    },

    async FINISH_ATTEMPT({ dispatch, commit, state }, payload) {
        await finishAttempt(state.currentAttempt.id)
                    .then((response) => {
                        state.currentAttempt = response;

                        commit('SET_QUESTIONS_ATTEMPT', response);



                        dispatch('GET_QUESTIONS_ATTEMPT', state.currentAttempt.id)
                    })
    },

    async REMOVE_QUESTION_RULE({ state }, payload) {
        try {
            const data = await removeQuestionRule(payload);
            state.test[0].questions = state.test[0].questions.filter(el => el.id !== payload.question_id);
            return data;
        } catch (e) {
            throw e;
        }
    },

    async GET_CURRENT_QUESTION({state}) {
        try {
            //if (state.test.attempt.status == 'ACTIVE'){
                const response = await axios.post(`/api/v2/quiz-attempt-questions/${state.currentQuestion.id}`, {
                    _method: 'PATCH',
                    action: 'set-current-question',
                });
                console.log(response.data.data.server_time);
                state.server_time = new Date(response.data.data.server_time.replace(/-/g, '/'));
            //}
        } catch (e) {
            console.warn(e);
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};

