import axios from '@base/bootstrap';

const data = {
    organizations: {},
    organization: {},
    coords: [51.660560, 39.200079],
    permissions: {},
    hasRole: [],
    teachers: [],
    schedule: [],
};

const getters = {
    ORGANIZATIONS: (state) => state.organizations,
    ORGANIZATION: (state) => state.organization,
    COORDS: (state) => state.coords,
    PERMISSIONS: (state) => state.permissions,
    TEACHERS: (state) => state.teachers,
    HAS_ROLE: (state) => state.hasRole,
    SCHEDULE: (state) => state.schedule,
};

const mutations = {
    SET_ORGANIZATIONS(state, payload) { // eslint-disable-line
        state.organizations = { ...payload }; // eslint-disable-line
    },
    SET_SCHEDULE: (state, payload) => {
        state.schedule = payload; // eslint-disable-line
    },
};

const actions = {
    async GET_SCHEDULE(context, payload) {

        await axios.get(`/api/v1/organizations/${payload.id}/schedules`)
            .then((response) => {
                context.commit('SET_SCHEDULE', response.data.data.schedules);

            })
            .catch(() => {

            });
    },
    async GET_ORGANIZATIONS(context, payload) {


        const formData = {
            qty: payload.per_page,
            page: payload.page,
            show: 1,
            type: payload.type,
            with: payload.with,
            district: payload.district,
            can_iom: payload.can_iom
        };

        await axios.get(`/api/v1/organizations`, {params: formData})
            .then((response) => {

                context.commit('SET_ORGANIZATIONS', response.data.data.organizations.data);
            })
            .catch(() => {

            });
    },
    async GET_ORGANIZATION({state}, payload) {
        await axios.get(`/api/v1/organizations/${payload.id}`)
            .then((response) => {
                state.organization = response.data.data.organization;
                state.permissions = response.data.data.permissions;
                state.teachers = response.data.data.teachers;
                state.hasRole = response.data.data.has_role;
                state.coords = [response.data.data.organization.map_coordinates_x,
                                response.data.data.organization.map_coordinates_y];
            })
    },
};

export default {
    namespaced: true,
    state: data,
    mutations,
    getters,
    actions,
};
