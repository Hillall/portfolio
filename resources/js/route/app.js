// Bashinsky
import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from '../bootstrap';
import store from '../store/app';

const Page = () => import( '../components/staticPage/Index.jsx');
const AdminStaticPage = () => import( '../components/admin/staticpage/index.vue');

const MainRegisterIndex = () => import( '../components/main/register/Index.vue');
const MainLoginIndex = () => import( '../components/main/login/Index.vue');
const NewsList = () => import( '../components/news/List.vue');
const NewsShow = () => import( '../components/news/Show.vue');

const PortfolioShow = () => import( '../components/portfolio/Show.vue');

const Error404 = () => import( '../components/main/error/404.vue');
const MarketplaceIndex = () => import( '../components/marketplace/Index.vue');
const MarketplaceOrder = () => import( '../components/marketplace/Order.vue');

const MarketplaceEvent = () => import( '../components/course/Event.vue');
const MarketplaceEventContent = () => import( '../components/course/EventContent.vue');

const OrgList = () => import( '../components/org/List.vue');
const OrgEduIndex = () => import( '../components/org/edu/Index.vue');
const OrgEduTabContact = () => import( '../components/org/edu/tab/Contact.vue');
const OrgEduTabProgram = () => import( '../components/org/edu/tab/Program.vue');
const OrgEduTabCourse = () => import( '../components/org/edu/tab/Course.vue');

const OrgEduTabUsersNav = () => import( '../components/org/base/users/Nav');
const OrgEduTabUser = () => import( '../components/org/base/users/OrgTabUsersIndex');
const OrgEduTabApproveData = () => import( '../components/org/base/users/ApproveData' )

const OrgEduTabSchedule = () => import( '../components/org/edu/tab/Schedule.vue');
const OrgEduTabDatabase = () => import( '../components/org/edu/tab/Database.vue');
const OrgEduTabLectureRoom = () => import( '../components/org/edu/tab/LectureRoom.vue');

const OrgEduTestAccessIndex = () => import( '../components/org/base/tests/OrgEduTestAccess');

const OrgEduTabInterview = () => import( '../components/org/edu/tab/interview/TabInterview.vue');
const OrgEduEnterviewAttempt = () => import( '../components/org/edu/tab/interview/Attempt.vue');
const OrgEduInterviewSettings = () => import( '../components/org/edu/tab/interview/Settings.vue');
const InterviewIndex = () => import( '../components/org/edu/tab/interview/Index.vue');



const OrgEduCourse = () => import('../components/course/Index');
const OrgEduCourseContent = () => import('../components/course/CourseContent.vue');
const OrgEduCourseGroups = () => import('../components/course/Groups.vue');
const OrgEduCourseGradebook = () => import('../components/course/gradebook/GradebookList');
const OrgEducourseBankQuestion = () => import('../components/course/bank-question/BankQuestion.vue');

const OrgTabDatabase = () => import('../components/org/base/Database/Database.vue');

const OrgBaseTabAbout = () => import('../components/org/base/About/About');

const OrgEduViewTask = () => import('../components/course/task/Task.vue');
const OrgEduViewProgram = () => import('../components/program/Program.vue');
const OrgEduViewProgramGroups = () => import('../components/program/Groups.vue');
const ProgramSchedules = () => import('../components/program/ProgramSchedules.vue');

const OrgQuestionBankNavigation = () => import('../components/org/base/question_bank/Navigation');
const OrgQuestionBankQuestionList = () => import('../components/org/base/question_bank/Index');
const OrgQuestionBankCategoriesEdit = () => import('../components/org/base/question_bank/CategoriesEdit');

const OrgTestsIndexNav = () => import('../components/org/base/tests/IndexNav');
const OrgTestsIndex = () => import('../components/org/base/tests/Index');
const OrgTestsAllResults = () => import('../components/org/base/tests/OrganizationTestsResults');
const OrgTestsAllReviewers = () => import('../components/org/base/tests/OrganizationTestsReviewers');


const OrgReviewTestsIndex = () => import('../components/org/base/review-test/Index');
const OrgResultTestsIndex = () => import('../components/org/base/result-test/Index');


const OrgTestsShow = () => import('../components/org/base/tests/Show');
const OrgTestsShowQuestions = () => import('../components/org/base/tests/Questions');
const OrgTestsShowIndicators = () => import('../components/org/base/tests/Indicators');
const OrgTestsShowAccess = () => import('../components/org/base/tests/TestsShowAccess');
const OrgTestsShowResultAccess = () => import('../components/org/base/tests/TestsShowResultAccess');

const OrgTestsResults = () => import('../components/org/base/tests/Results');

const OrgViroIndex = () => import('../components/org/viro/Index.vue');
const OrgViroTabAbout = () => import('../components/org/viro/tab/About.vue');
const OrgViroTabContact = () => import('../components/org/viro/tab/Contact.vue');
const OrgViroTabProgram = () => import('../components/org/viro/tab/Program.vue'); // eslint-disable-line
const OrgViroTabUser = () => import('../components/org/base/users/OrgTabUsersIndex');
const OrgViroTabDocuments = () => import('../components/org/viro/tab/Documents.vue');

const OrgCamerasTabs = () => import( '../components/org/edu/tab/cameras/tabs');
const OrgCamerasIndex = () => import( '../components/org/edu/tab/cameras/Index');
const OrgCamerasSchedule = () => import( '../components/org/edu/tab/cameras/Schedule');

const OrgProctoringIndex = () => import( '../components/org/base/proctoring/Index')
const OrgProctoringShow = () => import( '../components/org/base/proctoring/Show')

const ProfileIndex = () => import('../components/profile/Index.vue');
const ProfileMain = () => import('../components/profile/tab/MainPage');

const RoutesIndex = () => import('../components/routes/RoutesIndex.vue');
const RoutesShow = () => import('../components/routes/RoutesShow.vue');

const ProfileTabCourse = () => import('../components/profile/tab/Course.vue');
const ProfileTabCompany = () => import('../components/profile/tab/Company.vue');
const ProfileTabPortfolio = () => import('../components/profile/tab/Portfolio.vue');
const ProfileTabEduProgram = () => import('../components/profile/tab/EduProgram.vue');
const ProfileTabChat = () => import('../components/profile/tab/Chat.vue');
const ProfileTabSetting = () => import('../components/profile/tab/ProfileTabSetting.vue');
const ProfileTabAdmin = () => import('../components/profile/tab/Admin.vue');
const ProfileTabVacancies = () => import('../components/profile/tab/Vacancies.vue');
const ProfileTabTests = () => import('../components/profile/tab/Tests.vue');
const ProfileTabCameraReviewer = () => import('../components/profile/tab/CameraReviewer.vue');
const ProfileProfile = () => import('../components/profile/tab/Profile.vue');
const ProfileProfileAbout = () => import('../components/profile/tab/profile/About.vue');
const ProfileProfileData = () => import('../components/profile/tab/profile/OrganizationData.vue');
const ProfileProfileTesting = () => import('../components/profile/tab/profile/Testing.vue');
const ProfileProfileDocuments = () => import('../components/profile/tab/profile/Documents.vue');



const ProfileTabEduOrganization = () => import('../components/profile/tab/EduOrganization.vue'); // eslint-disable-line max-len
const ProfileTabDistricts = () => import('../components/profile/tab/Districts.vue'); // eslint-disable-line max-len

const Admin = () => import('../components/admin/Index.vue');

const AdminSettingsIndex = () => import('../components/admin/settings/AdminSettingsIndex')


const AdminQuestionBank = () => import('../components/admin/question_bank/Index');
// const AdminQuestionBankQuestions = () => import('../components/admin/question_bank/Questions');
// const AdminQuestionBankCategories = () => import('../components/admin/question_bank/Categories');

const AdminTestsIndex = () => import('../components/admin/tests/Index');
const AdminTestsShow = () => import('../components/admin/tests/Show');
const AdminTestsShowQuestions = () => import('../components/admin/tests/Questions');
const AdminTestsShowIndicators = () => import('../components/admin/tests/Indicators');

const AdminIndicatorsIndex = () => import('../components/admin/indicators/Index');
const AdminIndicatorShow = () => import('../components/admin/indicators/Show');

const AdminIom = () => import('../components/admin/iom/Index');
const AdminIomForms = () => import('../components/admin/iom/Forms');
const AdminIomForm = () => import('../components/admin/iom/Form');

const AdminUsersNav = () => import( '../components/admin/users/Nav.vue');
const AdminUsersList = () => import( '../components/admin/users/Index.vue');
const AdminUserCardTemplates = () => import( '../components/admin/users/CardTemplateIndex.vue');
const AdminUserShow = () => import( '../components/admin/users/Show.vue');

const AdminOrganizationIndex = () => import( '../components/admin/organization/OrganizationsIndex.vue');
const AdminOrganizationsList = () => import( '../components/admin/organization/OrganizationsList.vue');
const AdminOrganizationOrders = () => import( '../components/admin/organization/AdminOrganizationOrders.vue');

const AdminDistrictsList = () => import( '../components/admin/organization/DistrictsList.vue');
const AdminOrganizationPopularity = () => import( '../components/admin/organization/Popularity.vue'); // eslint-disable-line max-len
const AdminOrganizationCamerasList = () => import( '../components/admin/organization/CamerasList.vue');
const AdminOrganizationTypes = () => import( '../components/admin/organization/AdminOrganizationTypes.vue');
const AdminOrganizationCamerasIntervals = () => import( '../components/admin/organization/AdminOrganizationIntervals.vue');
const AdminOrganizationCamerasViolations = () => import( '../components/admin/organization/CamerasViolations.vue');

const AdminStatistics = () => import( '../components/admin/statistics/Index.vue');
const AdminStatisticsGeneral = () => import( '../components/admin/statistics/general/Index.vue');
const AdminStatisticsEducations = () => import( '../components/admin/statistics/educations/Index.vue');
const AdminStatisticsEmployers = () => import( '../components/admin/statistics/employers/Index.vue');

const AdminDBStructure = () => import( '../components/admin/database/Struct.vue');
const AdminDBCategory = () => import( '../components/admin/database/Category.vue');
const AdminDBTable = () => import( '../components/admin/database/Table.vue');

const AdminRole = () => import( '../components/admin/role/Index.vue');
const AdminCompetencies = () => import( '../components/admin/competence/index.vue');

const Test = () => import( '../components/course/test/Index.vue');
const TestAttempt = () => import( '../components/course/test/TestAttempt.vue');
const TestSettings = () => import( '../components/course/test/TestSettings.vue');
const TestResult = () => import( '../components/course/test/TestResult.vue');
const TestCustomScales = () => import( '../components/course/test/TestCustomScales.vue');

const MiudIndex = () => import( '../components/profile/tab/miud/Index');


const Chat = () => import('../components/chat/Index.vue');

const FormulaConstructor = () => import('../components/formula-constructor/FormulaConstructor.vue')

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'main',
        component: Page,
        props: {
            typePage: 'MAIN',
        }
    },
    {
        path: '/login',
        name: 'login',
        component: MainLoginIndex,
    },
    {
        path: '/register',
        name: 'register',
        component: MainRegisterIndex,
    },
    {
        path: '/formula-constructor',
        name: 'formula-constructor',
        component: FormulaConstructor,
    },
    {
        path: '/news/list/:page?',
        name: 'news.list',
        component: NewsList,
    },
    // {
    //     path: '/marketplace/makeorder',
    //     name: 'marketplace.makeOrderMain',
    //     component: MarketplaceMakeOrder,
    // },
    {
        path: '/news/:id',
        name: 'news.show',
        component: NewsShow,
    },
    {
        path: '/portfolio/:id/:page?',
        name: 'portfolio.show',
        component: PortfolioShow,
    },
    {
        path: '/organization/:type/list',
        name: 'org.list',
        component: OrgList,
    },

    {
        path: '/organization/edu/:id/education-program/:id_program',
        name: 'org.edu.programview',
        component: OrgEduViewProgram,
    },
    {
        path: '/organization/edu/:id/education-program/:id_program/groups',
        name: 'org.edu.programview.groups',
        component: OrgEduViewProgramGroups,
    },
    {
        path: '/organization/edu/:id/education-program/:id_program/schedules',
        name: 'org.edu.programview.schedules',
        component: ProgramSchedules,
    },
    // {
    //     path: '/organization/edu/:organization_id/course/:course_id',
    //     name: 'org.edu.course',
    //     component: OrgEduCourse,
    //     props: (route) => ({ organization_id: Number(route.params.organization_id), course_id: Number(route.params.course_id) }),
    //     children: [
    //         {
    //             path: '/',
    //             name: 'org.edu.course.view',
    //             component: OrgEduCourseContent,
    //         },
    //         {
    //             path: 'gradebook',
    //             name: 'org.edu.course.gradebooklist',
    //             component: OrgEduCourseGradebook,
    //         },
    //         {
    //             path: 'bank-question/:category_id?',
    //             name: 'org.edu.course.bank-question',
    //             props: (route) => ({ course_id: Number(route.params.course_id) }),
    //             component: OrgEduViewBankQuestion,
    //         },
    //         {
    //             path: 'groups',
    //             name: 'org.edu.course.groups',
    //             component: OrgEduCourseGroups,
    //         },
    //     ],
    // },
    {
        path: '/organization/edu/:organization_id/course/:course_id',
        name: 'org.edu.course',
        component: OrgEduCourse,
        redirect: { name: 'org.edu.course.view' },
        props: (route) => ({ organization_id: Number(route.params.organization_id), course_id: Number(route.params.course_id) }),
        children: [
            {
                path: '/',
                name: 'org.edu.course.view',
                component: OrgEduCourseContent,
            },
            {
                path: 'gradebook',
                name: 'org.edu.course.gradebooklist',
                component: OrgEduCourseGradebook,
            },
            {
                path: 'groups',
                name: 'org.edu.course.groups',
                props: (route) => ({ organization_id: Number(route.params.organization_id), course_id: Number(route.params.course_id) }),
                component: OrgEduCourseGroups,
            },
            {
                path: 'bank-question/:category_id?',
                name: 'org.edu.course.bank-question',
                props: (route) => ({ course_id: Number(route.params.course_id) }),
                component: OrgEducourseBankQuestion,
            },
            {
                path: 'task/:task_id',
                name: 'org.edu.program.course.task',
                component: OrgEduViewTask,
            },
            {
                path: 'test/:test_id',
                name: 'org.edu.course.test',
                redirect: { name: 'org.edu.course.test.results' },
                component: Test,
                children: [
                    {
                        path: 'attempt/:attempt_id',
                        name: 'org.edu.course.test.attempt',
                        component: TestAttempt,
                    },
                    {
                        path: 'settings',
                        name: 'org.edu.course.test.settings',
                        component: TestSettings,
                    },
                    {
                        path: 'results',
                        name: 'org.edu.course.test.results',
                        component: TestResult,
                    },
                    {
                        path: 'scales',
                        name: 'org.edu.course.test.scales',
                        component: TestCustomScales,
                    },
                ],
            },
        ],
    },
    {
        path: '/organization/edu/:id',
        name: 'org.edu.show',
        component: OrgEduIndex,
        meta: { showBaseTemplate: true },
        children: [
            {
                path: 'test_access',
                name: 'org.edu.test_access',
                component: OrgEduTestAccessIndex,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'routes',
                name: 'org.edu.routes',
                component: RoutesIndex,
                meta: { type: 'edu', showBaseTemplate: true },
                props: (route) => ({ organization_id: Number(route.params.id), nav_view: false }),
            },
            {
                path: 'routes/:route_id',
                name: 'org.edu.routes.show',
                component: RoutesShow,
                meta: { type: 'edu', showBaseTemplate: true },
                props: (route) => ({ route_id: Number(route.params.route_id), organization_id: Number(route.params.id) }),
            },
            {
                path: 'about',
                name: 'org.edu.about',
                component: OrgBaseTabAbout,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'courses/:page?',
                name: 'org.edu.tab.courses',
                component: OrgEduTabCourse,
                meta: { showBaseTemplate: true },
                props: (route) => ({ organization_id: Number(route.params.id) }),
            },
            {
                path: 'contact',
                name: 'org.edu.contact',
                component: OrgEduTabContact,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'education-programs/:page?',
                name: 'org.edu.program',
                component: OrgEduTabProgram,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'lecture-room',
                name: 'org.edu.lectureroom',
                component: OrgEduTabLectureRoom,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'interview',
                name: 'org.edu.interviews',
                component: OrgEduTabInterview,
                meta: { showBaseTemplate: true },
            },

            {
                path: 'interview/:test_id/',
                name: 'org.edu.interview',
                component: InterviewIndex,
                meta: { showBaseTemplate: false },
                children: [
                    {
                        path: 'attempt/:attempt_id',
                        name: 'org.edu.interview.attempt',
                        component: OrgEduEnterviewAttempt,
                    },
                    {
                        path: 'settings',
                        name: 'org.edu.interview.settings',
                        component: OrgEduInterviewSettings,
                    },
                    {
                        path: 'result',
                        name: 'org.edu.interview.result',
                        component: TestResult,
                    },
                ],
            },
            {
                path: 'user/',
                name: 'org.edu.user',
                component: OrgEduTabUsersNav,
                meta: { showBaseTemplate: true },
                redirect: { name: 'org.edu.user.list' },
                children: [
                    {
                        path: 'list',
                        name: 'org.edu.user.list',
                        meta: { showBaseTemplate: true },
                        component: OrgEduTabUser
                    },
                    {
                        path: 'approve_data',
                        name: 'org.edu.user.approve_data',
                        meta: { showBaseTemplate: true },
                        component: OrgEduTabApproveData
                    }
                ]
            },
            {
                path: 'schedule',
                name: 'org.edu.schedule',
                component: OrgEduTabSchedule,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'database/:table?/:page?',
                name: 'org.edu.database',
                component: OrgEduTabDatabase,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'result-test',
                name: 'org.edu.result-test',
                component: OrgResultTestsIndex,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'cameras/',
                name: 'org.edu.cameras',
                component: OrgCamerasTabs,
                meta: { showBaseTemplate: true },
                redirect: { name: 'org.edu.cameras.index' },
                children: [
                    {
                        path: 'index',
                        name: 'org.edu.cameras.index',
                        meta: { showBaseTemplate: true },
                        props: (route) => ({ organization_id: Number(route.params.id) }),
                        component: OrgCamerasIndex,
                    },
                    {
                        path: 'schedule',
                        name: 'org.edu.cameras.schedule',
                        meta: { showBaseTemplate: true },
                        props: (route) => ({ organization_id: Number(route.params.id) }),
                        component: OrgCamerasSchedule,
                    }
                ]
            },

            {
                path: 'proctoring',
                name: 'org.edu.proctoring',
                component: OrgProctoringIndex,
                meta: { showBaseTemplate: true },
                props: (route) => ({ organization_id: Number(route.params.id) }),
            },
            {
                path: 'proctoring/:proctoring_id',
                name: 'org.edu.proctoring.show',
                component: OrgProctoringShow,
                meta: { showBaseTemplate: false },
                props: (route) => ({ organization_id: Number(route.params.id), proctoring_id:Number(route.params.proctoring_id) }),
            }
        ],
    },
    {
        path: '/organization/edu/:id/marketplace/event/:event_id',
        name: 'org.edu.event',
        component: MarketplaceEvent,
        redirect: { name: 'org.edu.event.view' },
        props: (route) => ({ event_id: Number(route.params.event_id), organization_id: Number(route.params.id) }),
        children: [
            {
                path: '/',
                name: 'org.edu.event.view',
                component: MarketplaceEventContent,
                props: (route) => ({ event_id: Number(route.params.event_id), organization_id: Number(route.params.id) }),
            },
        ]
    },

    {
        path: '/organization/viro/:id',
        name: 'org.viro.show',
        component: OrgViroIndex,
        props: (route) => ({ organization_id: Number(route.params.id)}),
        children: [
            {
                path: 'about/:page?',
                name: 'org.viro.about',
                component: OrgViroTabAbout,
            },
            {
                path: 'routes',
                name: 'org.viro.routes',
                component: RoutesIndex,
                meta: { type: 'viro' },
                props: (route) => ({ organization_id: Number(route.params.id), nav_view: false }),
            },
            {
                path: 'routes/:route_id',
                name: 'org.viro.routes.show',
                component: RoutesShow,
                meta: { type: 'viro' },
                props: (route) => ({ route_id: Number(route.params.route_id), organization_id: Number(route.params.id) }),
            },
            {
                path: 'about/:page?',
                name: 'org.VIRO.about',
                component: OrgViroTabAbout,
            },
            {
                path: 'contact',
                name: 'org.viro.contact',
                component: OrgViroTabContact,
            },
            {
                path: 'education-program/:page?',
                name: 'org.viro.program',
                component: OrgEduTabProgram,
            },
            {
                path: 'course',
                name: 'org.viro.course',
                component: OrgEduTabCourse,
                props: (route) => ({ organization_id: Number(route.params.id) }),
            },
            {
                path: 'user',
                name: 'org.viro.user',
                component: OrgEduTabUser,
                meta: { showBaseTemplate: true },
            },
            {
                path: 'documents/:page?',
                name: 'org.viro.documents',
                component: OrgViroTabDocuments,
            },
            {
                path: 'question_bank',
                name: 'org.viro.question_bank',
                component: OrgQuestionBankNavigation,
                redirect: {
                    name: 'org.viro.question_bank.questions.list',
                },
                children: [
                    {
                        path: 'questions',
                        name: 'org.viro.question_bank.questions.list',
                        component: OrgQuestionBankQuestionList,
                    },
                    {
                        path: 'category',
                        name: 'org.viro.question_bank.questions.category',
                        component: OrgQuestionBankCategoriesEdit,
                    },
                ],
            },
            {
                path: 'test',
                name: 'org.viro.test',
                component: OrgTestsIndexNav,
                redirect: {
                    name: 'org.viro.test.list',
                },
                children: [
                    {
                        path: 'list',
                        name: 'org.viro.test.list',
                        component: OrgTestsIndex,
                    },
                    {
                        path: 'results',
                        name: 'org.viro.test.all.results',
                        component: OrgTestsAllResults,
                    },
                    {
                        path: 'reviewers',
                        name: 'org.viro.test.all.reviewers',
                        component: OrgTestsAllReviewers,
                    }
                ]
            },
            {
                path: 'review-test',
                name: 'org.viro.review-test',
                component: OrgReviewTestsIndex,
            },
            {
                path: 'tests/:test_id',
                name: 'org.viro.test.show',
                component: OrgTestsShow,
                redirect: {
                    name: 'org.viro.test.questions',
                },
                children: [
                    {
                        path: 'questions',
                        name: 'org.viro.test.questions',
                        component: OrgTestsShowQuestions,
                    },
                    {
                        path: 'indicators',
                        name: 'org.viro.test.indicators',
                        component: OrgTestsShowIndicators,
                    },
                    {
                        path: 'access',
                        name: 'org.viro.test.access',
                        component: OrgTestsShowAccess,
                    },
                    {
                        path: 'result-access',
                        name: 'org.viro.test.result-access',
                        component: OrgTestsShowResultAccess,
                    },
                    {
                        path: 'results',
                        name: 'org.viro.test.results',
                        component: OrgTestsResults,
                    },
                ],
            },
        ],
    },
    {
        path: '/chats/:currentChatId?',
        name: 'me.chat',
        props: (route) => ({ currentChatId: route.params.currentChatId }),
        component: Chat,
    },
    {
        path: '/me',
        name: 'me',
        component: ProfileIndex,
        meta: { needAuth: true },
        children: [
            {
                path: 'main',
                name: 'me.main',
                component: ProfileMain
            },
            {
                path: 'routes',
                name: 'me.routes',
                component: RoutesIndex,
                meta: { type: 'profile' },
                props: (route) => ({ nav_view: true }),
            },
            {
                path: '/me/routes/marketplace',
                name: 'me.routes.marketplace.index',
                props: (route) => ({ attempt_id: route.query?.attempt_id }),
                component: MarketplaceIndex,
            },
            {
                path: '/me/routes/testing',
                name: 'me.routes.testing',
                component: ProfileProfileTesting
            },
            {
                path: '/me/routes/marketplace/order',
                name: 'me.routes.marketplace.order',
                component: MarketplaceOrder,
            },
            {
                path: 'routes/:route_id',
                name: 'me.routes.show',
                component: RoutesShow,
                props: (route) => ({ route_id: Number(route.params.route_id) }),
                meta: { type: 'profile' },
            },
            {
                path: 'course/:type?',
                name: 'me.course',
                component: ProfileTabCourse,
                props: (route) => ({ type: route.params.type }),
            },
            {
                path: 'company',
                name: 'me.company',
                component: ProfileTabCompany,
            },
            {
                path: 'edu-organization',
                name: 'me.edu-organization',
                component: ProfileTabEduOrganization,
            },
            {
                path: 'districts',
                name: 'me.districts',
                component: ProfileTabDistricts,
            },
            // {
            //     path: 'chat/:currentChatId?',
            //     name: 'me.chat',
            //     component: ProfileTabChat,
            // },
            {
                path: 'portfolio/:page?',
                name: 'me.portfolio',
                component: ProfileTabPortfolio,
            },
            {
                path: 'education-program',
                name: 'me.edu-program',
                component: ProfileTabEduProgram,
            },
            {
                path: 'profile',
                name: 'me.profile',
                component: ProfileProfile,
                redirect: { name: 'me.profile.about' },
                children: [
                    {
                        path: 'about',
                        name: 'me.profile.about',
                        component: ProfileProfileAbout
                    },
                    {
                        path: 'data',
                        name: 'me.profile.data',
                        component: ProfileProfileData
                    },
                    {
                        path: 'documents',
                        name: 'me.profile.documents',
                        component: ProfileProfileDocuments
                    }
                ]
            },
            {
                path: 'setting',
                name: 'me.setting',
                component: ProfileTabSetting,
            },
            {
                path: 'admin',
                name: 'me.admin',
                component: ProfileTabAdmin,
            }, {
                path: 'vacancies',
                name: 'me.vacancies',
                component: ProfileTabVacancies,
            },
            {
                path: 'tests',
                name: 'me.tests',
                component: ProfileTabTests,
            },
            {
                path: 'camera_reviewer',
                name: 'me.camera_reviewer',
                component: ProfileTabCameraReviewer,
            },


            {
                path: 'miud/:organization_id',
                name: 'me.edu-organization.miud',
                component: MiudIndex,
                props: (route) => ({ organization_id: Number(route.params.organization_id) }),
            },
        ],
    },
    {
        path: '/test/:test_id',
        name: 'test',
        redirect: { name: 'test.results' },
        component: Test,
        children: [
            {
                path: 'attempt/:attempt_id',
                name: 'test.attempt',
                component: TestAttempt,
            },
            {
                path: 'settings',
                name: 'test.settings',
                component: TestSettings,
            },
            {
                path: 'results',
                name: 'test.results',
                component: TestResult,
            },
            {
                path: 'scales',
                name: 'test.scales',
                component: TestCustomScales,
            },
        ],
    },
    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        meta: { needAuth: true },
        children: [
            {
                path: 'question_bank',
                name: 'admin.question_bank',
                component: AdminQuestionBank,
                // children: [
                //     {
                //         path: 'questions',
                //         name: 'admin.question_bank.questions',
                //         component: AdminQuestionBankQuestions
                //     },
                //     {
                //         path: 'categories',
                //         name: 'admin.question_bank.categories',
                //         component: AdminQuestionBankCategories
                //     },
                // ]
            },
            {
                path: 'settings',
                name: 'admin.settings.index',
                component: AdminSettingsIndex
            },
            {
                path: 'users',
                name: 'admin.users',
                component: AdminUsersNav,
                redirect: {
                    name: 'admin.users.list',
                },
                children: [
                    {
                        path: 'list',
                        name: 'admin.users.list',
                        component: AdminUsersList,
                    },
                    {
                        path: 'list/:user_id',
                        name: 'admin.users.list.show',
                        component: AdminUserShow,
                    },
                    {
                        path: 'card-templates',
                        name: 'admin.users.card-templates',
                        component: AdminUserCardTemplates,
                    },
                ],
            },
            {
                path: 'organization',
                name: 'admin.organization',
                component: AdminOrganizationIndex,
                redirect: { name: 'admin.organizationsList', component: AdminOrganizationsList },
                children: [
                    {
                        path: 'organizationsList',
                        name: 'admin.organizationsList',
                        component: AdminOrganizationsList,
                    },
                    {
                        path: 'orders',
                        name: 'admin.organizations.orders',
                        component: AdminOrganizationOrders,
                    },
                    {
                        path: 'districts',
                        name: 'admin.organization.districtsList',
                        component: AdminDistrictsList,
                    },
                    {
                        path: 'popularity',
                        name: 'admin.organization.popularity',
                        component: AdminOrganizationPopularity,
                    },
                    {
                        path: 'camerasList',
                        name: 'admin.organization.camerasList',
                        component: AdminOrganizationCamerasList,
                    },
                    {
                        path: 'types',
                        name: 'admin.organization.types',
                        component: AdminOrganizationTypes,
                    },
                    {
                        path: 'cameras_intervals',
                        name: 'admin.organization.cameras_intervals',
                        component: AdminOrganizationCamerasIntervals
                    },
                    {
                        path: 'cameras_violations',
                        name: 'admin.organization.cameras_violations',
                        component: AdminOrganizationCamerasViolations
                    }
                ]
            },
            {
                path: 'database/struct',
                name: 'admin.db.struct',
                component: AdminDBStructure,
            },
            {
                path: 'database/table',
                name: 'admin.db.table',
                component: AdminDBTable,
            },
            {
                path: 'database/category',
                name: 'admin.db.category',
                component: AdminDBCategory,
            },
            {
                path: 'role',
                name: 'admin.role',
                component: AdminRole,
            },
            {
                path: 'competencies/:id?',
                name: 'admin.competence.child',
                component: AdminCompetencies,
            },
            {
                path: 'competencies/related/',
                name: 'admin.competence.related',
                component: AdminCompetencies,
            },
            {
                path: 'statistics',
                name: 'admin.statistics',
                component: AdminStatistics,
                children: [
                    {
                        path: 'general',
                        name: 'admin.statistics.general',
                        component: AdminStatisticsGeneral,
                    },
                    {
                        path: 'organizations',
                        name: 'admin.statistics.educations',
                        component: AdminStatisticsEducations,
                    },
                    {
                        path: 'employers',
                        name: 'admin.statistics.employers',
                        component: AdminStatisticsEmployers,
                    },
                ],
            },
            {
                path: 'tests',
                name: 'admin.tests.index',
                component: AdminTestsIndex,
            },
            {
                path: 'tests/:id',
                name: 'admin.tests.show',
                component: AdminTestsShow,
                redirect: {
                    name: 'admin.tests.show.questions',
                },
                children: [
                    {
                        path: 'questions',
                        name: 'admin.tests.show.questions',
                        component: AdminTestsShowQuestions,
                    },
                    {
                        path: 'indicators',
                        name: 'admin.tests.show.indicators',
                        component: AdminTestsShowIndicators,
                    },
                ],
            },
            {
                path: 'indicators',
                name: 'admin.indicators.index',
                component: AdminIndicatorsIndex,
            },
            {
                path: 'indicators/:id',
                name: 'admin.indicators.show',
                component: AdminIndicatorShow,
            },
            {
                path: 'iom',
                name: 'admin.iom',
                component: AdminIom,
                redirect: { name: 'admin.iom.forms' },
                children: [
                    {
                        path: 'forms',
                        name: 'admin.iom.forms',
                        component: AdminIomForms,
                    },
                    {
                        path: 'form/:form_id',
                        name: 'admin.iom.forms.show',
                        props: (route) => ({ form_id: Number(route.params.form_id) }),
                        component: AdminIomForm,
                    },
                    {
                        path: 'routes',
                        name: 'admin.iom.routes',
                        component: RoutesIndex,
                        meta: { type: 'admin' },
                    },
                    {
                        path: 'routes/:route_id',
                        name: 'admin.iom.routes.show',
                        component: RoutesShow,
                        props: (route) => ({ route_id: Number(route.params.route_id) }),
                        meta: { type: 'admin' },
                    },
                ]
            },
            {
                path: 'staticpage',
                name: 'admin.staticpage',
                component: AdminStaticPage,
            },

        ],
    },
    {
        path: '*',
        name: 'error.404',
        component: Error404,
    },
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

router.afterEach((to, from) => {
    if (!store.getters.auth) {
        store.dispatch('getAuth').then(() => {
        });
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.needAuth) && !store.getters.auth) {
        store.dispatch('getAuth').then(() => {
            next();
        });
    } else {
        next();
    }
});

export default router;
